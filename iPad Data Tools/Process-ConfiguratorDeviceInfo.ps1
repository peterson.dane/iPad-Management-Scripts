# This script takes in a CSV file from Configurator's Export function, processes
#   it, adds in some additional information from other parameters, and then
#   exports that info as a CSV file that's ready to be copy-pasted into the
#   iPad inventory.

[cmdletbinding()]
param (
    # If keeping Mandatory attribute, get rid of the "= $(throw ...)" so it just prompts?

    # FilePath parameter tells where .csv input file is
    [Parameter(Mandatory=$True)][string]$FilePath,

    # Model name/number string for the iPads. This will be entered in the CSV
    #   output file
    [Parameter(Mandatory=$True)][string]$Model,

    # Starting Asset Tag for this set of iPads. Will be incremented for each
    #   iPad processed and entered in CSV output file.
    [Parameter(Mandatory=$True)][int]$AssetTag,

    # Directory in which to output file with processed inventory info.
    [Parameter(Mandatory=$True)][int]$OutPath,
)

################################################################################
# Do a little bit of error checking first

# Check that the file actually exists
if (-not $(Test-Path $filePath)){
    throw "File `"$filePath`" does not exist"
}

# get file info
$checkFile = dir $filePath

# check to be sure extension is .csv; otherwise exit
if ($checkFile.Extension -ne ".csv") {
    throw "`"$filePath`" has wrong extension: `"$($checkFile.Extension)`""
}

# Make sure that file can be opened. If it won't, terminate w/ error
Try {
    $iPadList = Import-Csv $filePath
}
Catch {
    throw "File `"$filePath`" is not readable."
}

################################################################################

# Save original asset tag number for later use
$assetTagInitial = "$AssetTag"


Foreach ($iPad in $iPadList)
{
    # Create a new object
    $iPadInfo = New-Object -TypeName PSObject

    # Add members and structure the object to match inventory document
    # $iPadInfo | Add-Member -MemberType NoteProperty -Name "" -Value "$($)"
    $iPadInfo | Add-Member -MemberType NoteProperty -Name "Asset Tag" -Value "i$AssetTag"
    $iPadInfo | Add-Member -MemberType NoteProperty -Name "DeviceName" -Value "Student $AssetTag"
    $iPadInfo | Add-Member -MemberType NoteProperty -Name "Serial Number" -Value $iPad."Serial Number"
    $iPadInfo | Add-Member -MemberType NoteProperty -Name "Bldg" -Value ""
    $iPadInfo | Add-Member -MemberType NoteProperty -Name "First" -Value ""
    $iPadInfo | Add-Member -MemberType NoteProperty -Name "Last / Location" -Value ""
    $iPadInfo | Add-Member -MemberType NoteProperty -Name "Grade / Use" -Value ""
    $iPadInfo | Add-Member -MemberType NoteProperty -Name "MGPx" -Value ""
    $iPadInfo | Add-Member -MemberType NoteProperty -Name "Purchased" -Value ""
    $iPadInfo | Add-Member -MemberType NoteProperty -Name "Recycle 2020" -Value ""
    $iPadInfo | Add-Member -MemberType NoteProperty -Name "Model" -Value "$Model"
    $iPadInfo | Add-Member -MemberType NoteProperty -Name "Capacity" -Value $iPad.Capacity
    $iPadInfo | Add-Member -MemberType NoteProperty -Name "Wireless MAC" -Value $iPad."Wi-Fi Address"
    $iPadInfo | Add-Member -MemberType NoteProperty -Name "Bluetooth MAC" -Value $iPad."Bluetooth Address"
    $iPadInfo | Add-Member -MemberType NoteProperty -Name "UDID" -Value $iPad.UDID
    $iPadInfo | Add-Member -MemberType NoteProperty -Name "ECID" -Value $iPad.ECID

    # Add new object to array
    $iPadListProcessed += ,$iPadInfo

    # Increment AssetTag
    $AssetTag += 1
}

# Decrement Asset Tag by one for filename because of extra increment in loop
$AssetTag -= 1

# Construct path to output file
$outFile = Join-Path $OutPath "$OutFile - $assetTagInitial-$AssetTag.csv"

# Export processed iPad inventory data to CSV
$iPadListProcessed | Export-Csv -Path $outFile -NoTypeInformation -Encoding utf8
