# This script will take multiple device info CSV exports from Apple Configurator
#   and merge them into one file while looking up select inventory data from a
#   CSV export of the iPad inventory.
#
#

[cmdletbinding()]
param (
    # If keeping Mandatory attributes, get rid of the "= $(throw ...)" portion, so it just prompts?

    # Directory with miltiple Configurator device export CSV files
    [Parameter(Mandatory=$True)][string]$CEPath,

    # Path to CSV file exported from iPad inventory
    [Parameter(Mandatory=$True)][string]$InventoryPath,

    # Path to place output file at, including filename & extension
    [Parameter(Mandatory=$True)][string]$OutFile
)

function Test-Correctness
{
    param([string]$FilePath)

    if (-not $(Test-Path $FilePath)){
        throw "File `"$FilePath`" does not exist"
    }

    $checkFile = dir $filePath

    # check to be sure extension is .csv; exit if not
    if ($checkFile.Extension -ne ".csv") {
        throw "`"$filePath`" has wrong extension: `"$($checkFile.Extension)`""
    }

    # Make sure that file can be opened. If it doesn't, terminate w/ error
    Try {
        $importList = Import-Csv $filePath
    }
    Catch {
        throw "File `"$filePath`" is not readable."
    }

    return $importList
}



$iPadList = Test-Correctness -FilePath $CEPath
$inventory = Test-Correctness -FilePath $InventoryPath


################################################################################
################################################################################


Foreach ($iPad in $iPadList)
{
    # Create a new object
    $iPadInfo = New-Object -TypeName PSObject

    $iPadInventoryData = $( $inventory | Where-Object -Property "Serial Number" -eq -Value "$($iPad."Serial Number")")

    # Add members and structure the object to match inventory document
    # $iPadInfo | Add-Member -MemberType NoteProperty -Name "" -Value "$($)"
    $iPadInfo | Add-Member -MemberType NoteProperty -Name "Asset Tag" -Value $iPadInventoryData."Asset Tag"
    $iPadInfo | Add-Member -MemberType NoteProperty -Name "DeviceName" -Value "SL $($iPadInventoryData."Asset Tag")"
    $iPadInfo | Add-Member -MemberType NoteProperty -Name "Serial Number" -Value $iPad."Serial Number"
    $iPadInfo | Add-Member -MemberType NoteProperty -Name "Bldg" -Value ""
    $iPadInfo | Add-Member -MemberType NoteProperty -Name "First" -Value ""
    $iPadInfo | Add-Member -MemberType NoteProperty -Name "Last / Location" -Value ""
    $iPadInfo | Add-Member -MemberType NoteProperty -Name "Grade / Use" -Value ""
    $iPadInfo | Add-Member -MemberType NoteProperty -Name "Purchased" -Value ""
    $iPadInfo | Add-Member -MemberType NoteProperty -Name "Model" -Value "$Model"
    $iPadInfo | Add-Member -MemberType NoteProperty -Name "Capacity" -Value $iPad.Capacity
    $iPadInfo | Add-Member -MemberType NoteProperty -Name "Wireless MAC" -Value $iPad."Wi-Fi Address"
    $iPadInfo | Add-Member -MemberType NoteProperty -Name "Bluetooth MAC" -Value $iPad."Bluetooth Address"
    $iPadInfo | Add-Member -MemberType NoteProperty -Name "UDID" -Value $iPad.UDID
    $iPadInfo | Add-Member -MemberType NoteProperty -Name "ECID" -Value $iPad.ECID

    # Add new object to array
    $iPadListProcessed += ,$iPadInfo
}


# Export array to CSV
$iPadListProcessed | Export-Csv -Path "$OutFile"
