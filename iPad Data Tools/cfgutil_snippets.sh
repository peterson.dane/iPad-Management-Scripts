###################################################################################
# CFGUTIL COMMANDS
#
# A few frequently-used commands, ready to copy-paste and edit
#
# Don't actually run this file as a script.
#
###################################################################################


###################################################################################
# Get list of serial numbers as plain text
#   -f is "for all"
# This command can be run in PowerShell or natve macOS shell
cfgutil -f get serialNumber > "/path/to/output_file.txt"


###################################################################################
# Get list of serial numbers in JSON format
#   -f flag is "for all"
# This command can be run in PowerShell or natve macOS shell
cfgutil --format JSON -f get serialNumber > "/path/to/output_file.json"


###################################################################################
# Get list of serial numbers in JSON format and name file with current datetime
#   -f flag is "for all"

# This version is meant to be run in PowerShell ONLY.
cfgutil --format JSON -f get serialNumber > "/path/to/output_file_$(Get-Date -Format yyyy-MM-dd_HH.mm).json"

# Bash/zsh version of the previous command
cfgutil --format JSON -f get serialNumber > "/path/to/output_file_$(date +%F_%H.%M).json"
