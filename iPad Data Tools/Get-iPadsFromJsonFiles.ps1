# This script takes in a CSV file from our iPad inventory and a path to a
#   directory containing a set of JSON files output from Apple Configurator's
#   `cfgutil` command line utility. At the macOS command line the command to
#   generate these files would look like this:
#
#       cfgutil --format JSON -f get serialNumber > "/path/to/output_file.json"
#
# The script then looks these iPads up in the inventory and it prints out the
#   asset tags for each iPad.
#
# The inventory CSV must contain at least the following columns:
#   `Serial Number` and `Asset Tag`.
#
# To be honest, I don't entirely remember why I wrote this at this point.
#   It certainly served some purpose, but I'm a bit mystified as to why it was
#   useful to get all the Asset Tag values for a set of iPads and print them
#   out. It was probably something related to inventory or a checklist for a
#   periodic checkup.

[cmdletbinding()]

param(
    # Path to a directory contaning one or more JSON output files from cfguitl
    [string]$JsonPath,

    # Path to CSV iPad inventory file
    #   Can add ` = "/path/to/file.csv"` at the end to have this default to a
    #   specific value.
    [string]$inventoryPath
)


if ( !([string]::IsNullOrWhiteSpace($JsonPath)) )
{
    # Check if the final character of the path is `\`. If it is, strip it out.
    if ( $jsonPath[($jsonPath.Length - 1)] -eq "\" )
    {
        $jsonPath = $jsonPath.Substring(0, ($jsonPath.Length - 1) )
    }

    $pathFiles = Get-ChildItem -Path "$JsonPath\*.json"

    foreach ( $file in $pathFiles )
    {
        # Import data from JSON export from Configurator
        $iPadJson = Get-Content $file.fullName
        $iPadInfo = $( $iPadJson | ConvertFrom-Json )

        # As `cfgutil` outputs the data, each property represents an iPad
        # Each property's name is the ECID of that iPad
        # Get all the properties of $iPadInfo.Output and then iterate over them
        # Using `PSObject` property to get to those properties as objects unto themselves
        foreach ($property in $iPadInfo.Output.PSObject.Properties)
        {
            # We don't want to get the error property that cfgutil puts in the JSON
            #   at a random spot. This happens even on entirely successful runs.
            if ( $property.Name -ne "Errors" )
            {
                $iPadList += ,$property.Value.serialNumber
            }
        }
    }
}


# Import inventory
$iPadInventory = Import-Csv $inventoryPath

foreach ( $iPadSerial in $iPadList )
{
    $iPadAT = $($iPadInventory | Where-Object "Serial Number" -like "$iPadSerial")."Asset Tag"

    $assetTagList +=,$iPadAT
}

$iPadAT | Sort-Object
