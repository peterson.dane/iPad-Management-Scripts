<#
Takes a JSON export from cfgutil as input and invokes a bypass of the activation
locks on those iPads --- at least in theory.

I don't know that I've ever actually had this work for me. I always end up
having to just download the codes using `Get-ActivationLockBypassCodes.ps1` and
copy-paste them into the Finder or iTunes after connecting the locked iPad.

cfgutil command to run to get the data we need from iPads connected to sync
station:
    cfgutil --format JSON -f get serialNumber
#>

param(
    # Path to JSON file exported from cfgutil
    [string]$FilePath
)


# We'll be using Intune PowerShell Graph SDK, so we need to authenticate for that
Connect-MSGraph

# Get device list from Intune
$IntuneDevices = Get-IntuneManagedDevice

# Import data from JSON export from Configurator
$iPadJson = Get-Content $FilePath
$iPadInfo = $( $iPadJson | ConvertFrom-Json )

# Get all the properties of $iPadInfo.Output and then iterate over them
# Using hidden PSObject property to get to those properties as objects unto themselves
foreach ($property in $iPadInfo.Output.PSObject.Properties)
{
    if ( $property.Name -ne "Errors" )
    { # We don't want to get the error output that cfgutil puts in there at a random spot
        $iPadList += ,$property.Value.serialNumber
    }
}

# Now, for each serial number, send the activation lock bypass command
foreach ( $serialNumber in $iPadList )
{
    # Lookkup device based on serial number
    $Device = $( $IntuneDevices | Where-Object serialNumber -eq "$serialNumber" )

    Invoke-IntuneManagedDeviceBypassActivationLock -managedDeviceId $Device.id

}
