# Disused Scripts

These were in use at one point, but not anymore. There's not a good reason to just trash them (maybe they'll be useful again or serve as a template for something), but they don't need to be cluttering up the main directory.


`Process-IntuneProfileDeployDetails.ps1` and `Clean-IntuneDeviceList.ps1` seem to largely duplicate each others' functionality, but they do produce some slightly different output. I don't recall why each was used at this point. Looking at the former, it seems to have been used to check on profile statuses on the devices (seeing if devices had failed profile installs), while the latter was about doing enrollment checkups.
