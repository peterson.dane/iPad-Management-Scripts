<#

This script should mostly be rendered moot by our `Retire-IntuneManagediPad`
scripts.

I don't think this script ever actually worked because of the same API
misdocumentation that gave me issues in `Invoke-FailedAppReinstall.ps1` at first.

However, if it's fixed, it may still occasionally be useful if we notice that we
have a significant number of licenses assigned to iPads which are no longer in
service.

#>

# This script will use a list of "problem iPads", devices which we've removed
#   from circulation or retired, but which still have app licenses assigned to
#   them in Intune. For some paid apps with limited quantities of licenses, this
#   is a big problem.
#
# POSSIBLE IMPROVEMENTS:
#
# - Make this operate based on serial numbers. Would need to look up deviceId
#   for each serial and keep a list of those. The mobileAppInstallStatus object
#   returned when we get the list of licensed devices has the deviceID and name,
#   but ID is always going to be the best way to work on these, if we can find it.
#       - Consider that this might not be possible, if we don't have an entry in
#           Intune for the device, anymore! I am writing this script to try to
#           deal with devices where that's the case. Maybe there's some other
#           good way to do this.
#

[cmdletbinding()]

param
(
    # Array of deviceId strings
    [string[]]$ProblemDevices,

    # Username to use to authenticate with the Intune API
    [string]$APIUsername

    # Show testing output
    [switch]$Testing,

    # Show mismatches, not just matches; only works if $Testing = $true
    [switch]$ShowMismatches,

    # Wait before/after each app is listed
    [switch]$WaitApp,

    # Wait after a matching device is found
    [switch]$WaitFoundDevice,

    # Required to actually perform the action of removing the licenses
    [switch]$TakeAction
)

# Graph REST API URLs
$baseUri = "https://graph.microsoft.com/beta"
$baseDeviceUri = "$baseUri/deviceManagement/managedDevices"


# We'll be using Intune PowerShell Graph SDK, so we need to authenticate for that
Connect-MSGraph

# # Wait until they've done that!
# Write-Host 'Press any key to continue...';
# $null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');

# Get an auth token for manual Graph API calls
$authToken = & "$PSScriptRoot\Get-AuthToken.ps1" -User $APIUsername


##############################################################################
# Retrieve VPP App List
#   https://docs.microsoft.com/en-us/graph/api/intune-apps-iosvppapp-list?view=graph-rest-1.0
##############################################################################

$uri = "$baseUri/deviceAppManagement/mobileApps"
$vppAppList = Invoke-RestMethod -Uri $uri -Headers $authToken -Method Get

##############################################################################
##############################################################################

##############################################################################
# Work on Device licenses
#
# REVOKE LICENSE
#   https://docs.microsoft.com/en-us/graph/api/intune-apps-iosvppapp-revokedevicelicense?view=graph-rest-beta
#
# GET LICENSE STATUSES BY DEVICE
#   https://docs.microsoft.com/en-us/graph/api/intune-apps-iosvppappassigneddevicelicense-list?view=graph-rest-betaa
#
# Info about mobileAppInstallStatus object
#   https://docs.microsoft.com/en-us/graph/api/intune-apps-mobileappinstallstatus-get?view=graph-rest-beta
##############################################################################

# We'll need to iterate over the whole list of VPP apps
foreach ( $vppApp in $vppAppList.value )
{

    if ( $Testing )
    {
        # Wait for input to continue
        Write-Host "";
        if ($WaitApp) { $null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown'); }

        Write-Host -ForegroundColor Red "#####################################################"
        Write-Host -ForegroundColor Red "displayName: $($vppApp.displayName)"
        Write-Host -ForegroundColor Red "appId:       $($vppApp.id)"
        Write-Host -ForegroundColor Red "#####################################################"
        # Wait for input to continue
        Write-Host "";
        if ($WaitApp) { $null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown'); }
    }

    # Make API call to get devices with licenses for this app
    $uri = $baseUri + "/deviceAppManagement/mobileApps/$($vppApp.id)/microsoft.graph.iosVppApp/assignedLicenses"
    $devicesLicensed = Invoke-RestMethod -Uri $uri -Headers $authToken -Method Get

    # Now, check those devices to see if any are in our list of problem ones
    foreach ( $device in $devicesLicensed.value )
    {
        # If the device name is in our naught list, then we need to revoke
        #   that device license
        if ( $problemDevices.Contains($device.managedDeviceId) )
        {
            # TESTING ########################################################
            if ( $Testing )
            {
                Write-Host -ForegroundColor Blue "#####################################################"
                # Write-Host "id:         $($device.id)"
                Write-Host "deviceName: $($device.deviceName)"
                Write-Host "deviceId:   $($device.managedDeviceId)"
                Write-Host "appName:    $($vppApp.displayName)"
                Write-Host "vppAppId:   $($vppApp.id)"
                Write-Host -ForegroundColor Blue "#####################################################"

                # Wait for input to continue
                Write-Host "";
                if ($WaitFoundDevice) { $null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown'); }
            }
            # DONE TESTING ###################################################

            if ( $TakeAction )
            {
                # Construct body of call
                $body = @{
                    managedDeviceId = "$($device.managedDeviceId)"
                    notifyManagedDevices = $true
                }

                $jsonBody = $body | ConvertTo-Json

                # Make API Call
                # $uri = $baseUri + "/deviceAppManagement/mobileApps/$($vppApp.id)/deviceStatuses/$($device.id)/app/revokeDeviceLicense"
                $uri = $baseUri + "/deviceAppManagement/mobileApps/$($vppApp.id)/revokeDeviceLicense"
                Invoke-RestMethod -Uri $uri -Headers $authToken -Method Post -Body $jsonBody -ContentType "application/json"
            }
        } else {
            # TESTING ########################################################
            if ( $Testing -and $ShowMismatches )
            {
                Write-Host -ForegroundColor DarkBlue "#####################################################"
                # Write-Host -ForegroundColor DarkGray "id:         $($device.id)"
                Write-Host -ForegroundColor DarkGray "deviceName: $($device.deviceName)"
                Write-Host -ForegroundColor DarkGray "deviceId:   $($device.managedDeviceId)"
                Write-Host -ForegroundColor DarkGray "vppAppId:   $($vppApp.id)"
                Write-Host -ForegroundColor DarkBlue "#####################################################"
                Write-Host "";
            }
            # DONE TESTING ###################################################
        }
    }
}

##############################################################################
##############################################################################
