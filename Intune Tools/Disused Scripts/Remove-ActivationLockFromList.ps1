<#

THIS SCRIPT HAS BEEN SUPERSEDED BY ONE THAT USES A JSON EXPORT FROM CFGUTIL:
Invoke-BypassActivationLock.ps1

#>

# This script takes in a list of return-separted iPad serial numbers and attempts
#   to revoke their activation locks.
#
# Just be aware that I've LITERALLY (LITERALLY) never seen the activation lock
#   bypass command work on either of the MDMs that we've used. You'll have
#   better luck using the script that downloads the bypass codes, and then
#   copy-pasting those into the Finder when you hook up a locked iPad. (Will
#   need to use iTunes for this on Windows.)
#

# Dependency: https://github.com/microsoft/Intune-PowerShell-SDK

# RegEx to get rid of cruft from cfgutil .txt output
####`iPad \(ECID: 0x[A-F0-9]*\): `# That last space is important!

# Text file with a list of return-separated iPad serial numbers.
$ListPath = ""

# We'll be using Intune PowerShell Graph SDK, so we need to authenticate for that
Connect-MSGraph

$IntuneDevices = Get-IntuneManagedDevice

$iPadList = Get-Content $ListPath

foreach ( $serialNumber in $iPadList )
{
    $IntuneDevices | ? serialNumber -eq "$serialNumber" | Invoke-IntuneManagedDeviceBypassActivationLock
}
