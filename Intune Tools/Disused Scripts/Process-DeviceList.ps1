<#
This script was used to do some enrollment checkups early on in our Intune use,
but it's not something that we use anymore.
#>

[cmdletbinding()]
param (
    # FilePath parameter tells where .csv input file is
    # Not sure about the Mandatory attribute's behavior in script. May want to try this out...
    # If keeping Mandatory attribute, get rid of the "= $(throw ...)" portion?
    [Parameter(Mandatory=$True)][string]$ExportPathA,
    [Parameter(Mandatory=$True)][string]$ExportPathB,
    [Parameter(Mandatory=$True)][string]$OutPath
)

function Test-Correctness
{
    param([string]$FilePath)

    if (-not $(Test-Path $FilePath)){
        throw "File `"$FilePath`" does not exist"
    }

    $checkFile = dir $filePath

    # check to be sure extension is .csv; exit if not
    if ($checkFile.Extension -ne ".csv") {
        throw "`"$filePath`" has wrong extension: `"$($checkFile.Extension)`""
    }

    # Make sure that file can be opened. If it doesn't, terminate w/ error
    Try {
        $importList = Import-Csv $filePath
    }
    Catch {
        throw "File `"$filePath`" is not readable."
    }

    return $importList
}



$listA = Test-Correctness -FilePath $ExportPathA
$listB = Test-Correctness -FilePath $ExportPathB


################################################################################
################################################################################


Foreach ($DeviceDataA in $listA)
{
    # Create a new object
    $DeviceInfo = New-Object -TypeName PSObject

    $DeviceDataB = $( $listB | Where-Object -Property "Device ID" -eq -Value "$($DeviceDataA."Device ID")")

    # Add members and structure the object to match inventory document
    # $DeviceInfo | Add-Member -MemberType NoteProperty -Name "" -Value "$($)"
    $DeviceInfo | Add-Member -MemberType NoteProperty -Name "Device name" -Value $DeviceDataA."Device name"
    $DeviceInfo | Add-Member -MemberType NoteProperty -Name "OS version" -Value $DeviceDataA."OS version"
    $DeviceInfo | Add-Member -MemberType NoteProperty -Name "Last check-in A" -Value $DeviceDataA."Last check-in"
    $DeviceInfo | Add-Member -MemberType NoteProperty -Name "Last check-in B" -Value $DeviceDataB."Last check-in"
    $DeviceInfo | Add-Member -MemberType NoteProperty -Name "Enrollment date" -Value $DeviceDataA."Enrollment date"
    $DeviceInfo | Add-Member -MemberType NoteProperty -Name "Supervised" -Value $DeviceDataA."Supervised"
    $DeviceInfo | Add-Member -MemberType NoteProperty -Name "Model" -Value $DeviceDataA."Model"
    $DeviceInfo | Add-Member -MemberType NoteProperty -Name "Serial number" -Value $DeviceDataA."Serial number"
    $DeviceInfo | Add-Member -MemberType NoteProperty -Name "Wi-Fi MAC" -Value $DeviceDataA."Wi-Fi MAC"
    $DeviceInfo | Add-Member -MemberType NoteProperty -Name "Azure AD Device ID" -Value $DeviceDataA."Azure AD Device ID"
    $DeviceInfo | Add-Member -MemberType NoteProperty -Name "Device ID" -Value $DeviceDataA."Device ID"

    # Add new object to array
    $DeviceListProcessed += ,$DeviceInfo
}


# Export array to CSV
$DeviceListProcessed | Export-Csv -Path "$outPath"
