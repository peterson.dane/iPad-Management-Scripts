<#
Script will take device export CSV from Intune and strip out the left-to-right
  mark character (Unicode 0x200E, UTF-8 e2 80 8e) that's embedded in one of
  the datetime values. This invisible was causing spreadsheet programs to not
  recognize the value as a datetime and to instead treat it as plain text.

It also combines that info with information from our inventory and will also
  pare down the exported CSV to make it less cluttered and easier to view
  without having to manually hide or delete columns. There's an option to
  simplify it even further as well, using the `-Simplify` parameter.

This script hasn't been used for a while. We used to use it to check up on
  enrollment by looking at the last sync time (last check-in) value, but I've
  further automated the gathering of that info, so we don't have to manually
  download the CSV from Intune anymore. It may still be useful at some point,
  though.

This script *doesn't* make use of the Intune PowerShell SDK.
#>


[cmdletbinding()]
param (
    # FilePath parameter tells where .csv input file is
    # Not sure about the Mandatory attribute's behavior in script. May want to try this out...
    # If keeping Mandatory attribute, get rid of the "= $(throw ...)" portion?
    [Parameter(Mandatory=$True)][string]$FilePath,

    # This should specify a path to a CSV export of the iPad inventory.
    [Parameter(Mandatory=$True)]$iPadInventoryPath,

    # This string should consist of only a single character. This character will
    #   be stripped out of the CSV file before it's used.
    # "$([char]0x200E)" type syntax can also be used here when giving values for this parameter
    [string]$RemoveCharacter = "$([char]0x200E)",

    # Strip out a huge quanity of pretty useless data
    [switch]$Simplify=$false,

    # Get owner info from iPad inventory. Only has any effect with -Simplify
    [switch]$OwnerInfo=$false
)

# This will be used to produce a simplified object to be exported
$memberListSimplify = "Device name",
                      "Serial number",
                      "Model",
                      "OS version",
                      "Last check-in",
                      "Enrollment date",
                      "Wi-Fi MAC",
                      "Device ID"

# This is a list of the properties to be included in a fuller export.
$memberListOwnerInfo = "Asset Tag",
                       "Device name",
                       "Serial number",
                       "First",
                       "Last",
                       "Grade",
                       "Model",
                       "OS version",
                       "Last check-in",
                       "Enrollment date",
                       "Wi-Fi MAC",
                       "Device ID"

# If we're going to look up owner info, get that info from the iPad Inventory
if ( $OwnerInfo )
{
    $iPadInventory = Import-Csv -Path "$iPadInventoryPath"
}

# Get the file info in order to use it later
$fileData = Get-Item $FilePath

# Get the contents of the file
$file = Get-Content $fileData

# Generate new filename, which will be used a couple times
$newFilePath = "$($fileData.DirectoryName)\$($fileData.BaseName)_PS-FIXED$($fileData.Extension)"

# Strip out the offending character, output to new file
# Output file is in same dir as input, just wit "_PS-FIXED" added before extension
$fileFixed = $file.replace("$RemoveCharacter","") |
    Out-File -Encoding UTF8 -FilePath "$newFilePath"

# If Simplify flag is used, import the new CSV and save only desired values to
# improve readability and usefulness of the output file for our purposes.
# Output file is in same dir as input, just wit "_PS-FIXED" added before extension
# This overwrites the file created above.
if ( $simplify )
{
    $deviceInfo = Import-Csv -Path "$newFilePath"

    Foreach ($device in $deviceInfo)
    {
        $deviceSimple = New-Object -TypeName PSObject

        # Check the last check-in date and see how long ago it was
        $elapsedCheckInTime = New-Timespan -Start $([datetime]"$($device."Last check-in")")


        # Only do this stuff if we're collecting the Owner Info
        if ( $OwnerInfo )
        {
            # Get info from inventory by matching serial numbers
            $deviceInfo = $( $iPadInventory | Where-Object -Property "Serial Number" -eq -Value $device."Serial number" )

            # Set list of info to add to device object
            $memberList = "First", "Last", "Grade", "Asset Tag"

            # Add info to device object
            Foreach ($member in $memberList)
            {
                $device | Add-Member -MemberType NoteProperty -Name "$member" -Value $deviceInfo."$member"
            }

            # Set member list to the version for -OwnerInfo flag
            $memberList = $memberListOwnerInfo

        }
        # Otherwise, just set the list to the value for the -Simplify flag
        else
        {
            $memberList = $memberListSimplify
        }

        # Add members and structure the object to match inventory document
        Foreach ($member in $memberList)
        {
            $deviceSimple | Add-Member -MemberType NoteProperty -Name "$member" -Value $device."$member"
        }

        # If it's not checked in in over a week, it needs attention
        $deviceSimple | Add-Member -MemberType NoteProperty -Name "Needs Attention" -Value ( $elapsedCheckInTime.Days -gt 7 )

        # Add this device's entry to an array of similar objects
        $deviceListSimple += ,$deviceSimple
    }

    # Save simplified device list over the original fixed CSV
    $deviceListSimple | Export-Csv -Path $newFilePath -NoTypeInformation
}
