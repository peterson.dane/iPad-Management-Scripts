# Script will take device export CSV from Intune and clean up the data.
# Strips out the left-to-right mark character (Unicode 0x200E, UTF-8 e2 80 8e).
#   This isn't done explicitly, but by casting the value as a [datetime].

[cmdletbinding()]
param (
    # FilePath parameter tells where .csv input file is
    [Parameter(Mandatory=$True)][string]$FilePath,

    # Path to the CSV file exported from Intune's device blade
    [Parameter(Mandatory=$True)][string]$IntuneDeviceListPath,

    # Path to a CSV export of the iPad Inventory.
    [Parameter(Mandatory=$True)][string]$iPadInventoryPath,

    #
    [switch]$FailedOnly
)

# Read in data from the above databases
$intuneDeviceList = Import-Csv -Path "$IntuneDeviceListPath"
$iPadInventory = Import-Csv -Path "$iPadInventoryPath"

# Get the file info in order to use it later
$fileData = Get-Item $FilePath

# Generate new filename
$newFilePath = "$($fileData.DirectoryName)\$($fileData.BaseName)_PS-FIXED$($fileData.Extension)"

# Import data
$deviceInfo = Import-Csv -Path "$FilePath"

Foreach ($device in $deviceInfo)
{
    # Control logic for whether or not to operate on each device
    # If failedOnly flag has been used, check if the device has failed,
    # If not, then just set the control flag to true
    if( $FailedOnly ) { $proceed = ( $device."Deployment Status" -eq "Failed" ) }
    else { $proceed = $true }

    if ( $proceed )
    {
        $deviceFixed = New-Object -TypeName PSObject

        # Convert the ISO 8601 date to something Excel can read, add as member
        # If empty/null just make it an empty string
        Write-Verbose "$($device."Last status update")"

        try {
            $dateFixed = $([datetime]"$($device."Last status update")")
        } catch {
            $dateFixed = ""
            Write-Verbose "Null value encountered; setting dateFixed to blank string"
        }

        # Add this corrected info to the device object
        $device | Add-Member -MemberType NoteProperty -Name "Last Status Update Fixed" -Value $dateFixed

        # Need to get the serial number, now. Compare Device name to get that info
        # from Intune's databases
        $deviceInfo = $( $intuneDeviceList | Where-Object -Property "Device name" -eq -Value $device.Device )

        # Add this serial number to the device's object
        $device | Add-Member -MemberType NoteProperty -Name "Serial Number" -Value $deviceInfo."Serial number"

        # Now, use the S/N to look up the student's info
        $deviceInfo = $( $iPadInventory | Where-Object -Property "Serial Number" -eq -Value $device."Serial Number" )

        # And add that information to the device object, Bluetooth
        $memberList = "First", "Last", "Grade", "Asset Tag"

        Foreach ($member in $memberList)
        {
          $device | Add-Member -MemberType NoteProperty -Name "$member" -Value $deviceInfo."$member"
        }

        # Look up email from Active Directory to help Marlene
        $deviceOwner = Get-ADuser -Filter "DisplayName -like '$($device.First) $($device.Last)'" -Property mail

        # Add that to the object, too
        $device | Add-Member -MemberType NoteProperty -Name "Email" -Value $deviceOwner.mail

        # List of properties for new object, in order
        $memberList = "Asset Tag",
                      "Device",
                      "First",
                      "Last",
                      "Grade",
                      "Email",
                      "Deployment Status",
                      "Serial Number",
                      "Last Status Update Fixed",
                      "Last status update",
                      "User Principal Name",
                      "Device ID"

        Foreach ($member in $memberList)
        {
          $deviceFixed | Add-Member -MemberType NoteProperty -Name "$member" -Value $device."$member"
        }

        # Add this device's entry to an array of similar objects
        $deviceListFixed += ,$deviceFixed
    }
}

# Save simplified device list over the original fixed CSV
$deviceListFixed | Export-Csv -Path $newFilePath -NoTypeInformation
