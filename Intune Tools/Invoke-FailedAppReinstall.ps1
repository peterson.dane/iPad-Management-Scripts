<#
This script makes a call to the Graph API to get all the VPP apps that have
devices with failed install states. It then iterates through that list, gets the
devices with failed install states, and revokes their licenses individually.

Because this script can take a long time to run, the authentication or auth
token might expire before it's done. In those instances, it'll try to renew
them. This will require interaction for the Get-AuthToken.ps1 call.

TODO: Add device-based filtering, either by serial number or deviceId (or both)
      so individual devices or device sets can be targeted.

TODO: Consider moving all the repeated try{}catch{} stuff into a function? This
      might not be worth the hassle? It would probably make things cleaner and
      easier to read, though, and it would avoid having that same statement
      duplicated all over the place.

#>

[cmdletbinding()]

param
(
    # List of VPP app IDs for apps that we want to sync, ignoring any others
    # These are Intune's internal UUID values, not BundleID values! Easiest
    #   place to get that UUID is from the URL of the app's page in Intune or
    #   via API call.
    #
    # If this parameter isn't passed, the script will operate on all apps.
    [string[]]$AppIds = @(),

    # Username to use to authenticate with the Intune API
    [string]$APIUsername,

    # Also pull devices with 'pendingInstall' status, not just 'failed'
    [switch]$IncludePending,

    # Wait after each app's info is listed. Wait is in seconds
    #   Wait of -1 implies manual advancement
    #   Wait of  0 will produce no wait
    #   only applies if $Commentary = $true
    [int32]$WaitApp = 0,

    # Wait after each device's info is listed. Wait is in seconds
    #   Wait of -1 implies manual advancement
    #   Wait of  0 will produce no wait
    #   only applies if $Commentary = $true
    [int32]$WaitDevice = 0,

    # Wait after revoking license to send sync command to give some breathing room
    #   defaults to 5 seconds
    [uint32]$WaitSync = 5,

    # Show testing output
    [switch]$Commentary,

    # Required to actually perform the action of removing the licenses
    [switch]$TakeAction
)

################################################################################
# Similar to cmd `pause`; wait for any key to be pressed to continue
function Invoke-Pause {
    # Default to empty message string
    param( [string]$Message = "" )

    # Write message
    Write-Host -ForegroundColor Yellow "$Message";

    # Wait for input to continue
    $null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');
}
################################################################################

# Intune API URLs
$baseUri = "https://graph.microsoft.com/beta"
$baseAppUri = $baseUri + "/deviceAppManagement/mobileApps/"


# We'll be using Intune PowerShell Graph SDK, so we need to authenticate for that
Connect-MSGraph | Out-Null

# Get an auth token for manual Graph API calls
$authToken = & "$PSScriptRoot\Get-AuthToken.ps1" -User $APIUsername


################################################################################
# Retrieve VPP App List
# See Invoke-FailedAppReinstall.md for mor information about this section as
#   well as additional formats for the URI for the API call which might be handy.
#   https://docs.microsoft.com/en-us/graph/api/intune-apps-iosvppapp-list?view=graph-rest-1.0
#
# Using some filtering to select only apps with failed installs and to winnow
#   down to only iOS VPP apps. Could also do this later, and omit this filtering
# MUST include the `&select` portion, though, as by default, Graph doesn't
#   return the `installSummary`. If using `select`, then we have to select each
#   property that we need returned to us.
# URI has spaces in it!! No returns, though. I tried to break it up to be more readable.
# WATCH OUT for the spaces at the end of strings; those are important!!


if ( $IncludePending )
{ # Then we need to include pendingInstallDeviceCount in our filter
    $uri = $baseAppUri + `
            "?filter=(installSummary/failedDeviceCount gt 0 or " + `
            "installSummary/pendingInstallDeviceCount gt 0) " + `
            "and isof('microsoft.graph.iosVppApp')"+ `
            "&expand=installSummary&select=id,displayName"
            
} else { # Otherwise, we can just get failed devices
    $uri = $baseAppUri + `
            "?filter=installSummary/failedDeviceCount gt 0 " + `
            "and isof('microsoft.graph.iosVppApp')"+ `
            "&expand=installSummary&select=id,displayName"
}

$vppFailedAppList = Invoke-RestMethod -Uri $uri -Headers $authToken -Method Get
##############################################################################

# Set up counter for devices which were acted upon, just to give some rough
#   numbers for comparison between runs.
$deviceCount = 0

Write-Host ""

# We'll need to iterate over the whole list of VPP apps
foreach ( $vppApp in $vppFailedAppList.value )
{

    # If a list of app IDs were provided, then we need to check against that and
    #   skip any non-members.

    # If app IDs were provided, there will be more than zero, so check against
    #   that list.
    if ( $AppIds.Count -gt 0 )
    {
        # And if this app's ID isn't in the list, just skip it for now
        if ( !$AppIds.Contains($vppApp.id) )
        {
            if ( $Commentary ) # Give commentary, if the flag is set
            {
                Write-Host ""
                Write-Host -ForegroundColor DarkRed "Skipping $($vppApp.displayName)..."
                Write-Host ""
            }
            # Then move on to the next vppApp in the array
            Continue
        }
    }

    # If the flag was given, write out the commentary
    if ( $Commentary )
    {
        Write-Host -ForegroundColor Cyan "#####################################################"
        Write-Host -ForegroundColor Cyan "displayName: $($vppApp.displayName)"
        Write-Host -ForegroundColor Cyan "appId:       $($vppApp.id)"
        Write-Host -ForegroundColor Cyan "#####################################################"

        # Wait for input to continue or wait the defined number of seconds and continue
        if ( $WaitApp -lt 0 )
        {
            Write-Host ""
            Invoke-Pause -Message "Press any key to continue..."
            Write-Host ""
        } else {
            Write-Host ""
            Start-Sleep -Seconds $WaitApp
        }
    }

    ############################################################################
    # Make API call to get list of devices with failed install states for this app.
    #
    # Filter out any device with error code -2016330849 (0x87D13B9F as signed int32)
    #   This app corresponds to an error that an update is needed, but doesn't
    #   seem to indicate that the app isn't installed, per our limited testing.
    #   So we don't currently want these to be forced to uninstall and reinstall.
    #
    # TODO: Consider revisiting whether or not to invoke a reinstall for failed
    #       updates, too --- maybe as a switch parameter?
    #

    # Construct the URI to include/exclude devices as defined by our switch parameter.
    if ( $IncludePending )
    {
        # This version also gets devices with a 'pendingInstall' status, in case we
        #   end up needing to run it on them. It's all tested and working.
        $uri = $baseAppUri + $vppApp.id + "/deviceStatuses" + `
            "?filter=( (installState eq 'failed') or (installState eq 'pendingInstall') ) " + `
            "and (errorCode ne -2016330849)"

    } else {
        # This version only gets devices with 'failed' status
         $uri = $baseAppUri + $vppApp.id + "/deviceStatuses" + `
            "?filter=(installState eq 'failed') and (errorCode ne -2016330849)"
    }

    # Actually make the API call to get our device statuses for this app
    #
    # The try/catch will automatically try again if the call fails.
    # If the call fails twice, it'll try renewing the auth tokens.
    # If the call fails five times, it'll skip it.
    $tryCount = 0
    while ( $tryCount -lt 5 )
    {
        try
        {
            $deviceStatuses = Invoke-RestMethod -Uri $uri -Headers $authToken -Method Get
            $tryCount = 5
        }
        catch
        {
            $tryCount++
            $deviceList = $null

            if ($Commentary) { Write-Host -ForegroundColor Yellow "API Failure: $tryCount" }

            # If it has tried more than once, renew auth tokens; they're probably expired
            if ( $tryCount -gt 1 )
            {
                # Renew tokens
                if ($Commentary) { Write-Host -ForegroundColor Yellow "Renewing auth tokens" }
                Connect-MSGraph | Out-Null
                $authToken = & "$PSScriptRoot\Get-AuthToken.ps1" -User $APIUsername
            }
            Start-Sleep -Seconds 5
        }
    }
    ############################################################################

    ############################################################################
    # Quick command to show the previous results in tabluar format when testing:
    #
    #   $deviceStatuses.value | Format-Table -Property deviceName,installState,errorCode
    ############################################################################
    # Note: the following will display the errorCode as hex. Probably don't want
    #   to mess with that, aside from display. Just compare the signed int32 values.
    #
    #   '{0:X8}' -f $deviceStatuses.value[0].errorCode
    ############################################################################

    ############################################################################
    # Now, check those devices to see if any are in our list of problem ones
    foreach ( $device in $deviceStatuses.value )
    {
        # Increment the device counter so we can give an account at the end
        $deviceCount++

        # If the flag was given, write out the commentary
        if ( $Commentary )
        {
            Write-Host -ForegroundColor Blue "#####################################################"
            # Write-Host "id:         $($device.id)"
            Write-Host "deviceName:   $($device.deviceName)"
            Write-Host "deviceId:     $($device.deviceId)"
            Write-Host "installState: $($device.installState)"
            Write-Host "errorCode:    $($device.errorCode)"
            Write-Host ("errorCodeHex: 0x" + ('{0:X8}' -f $device.errorCode ))
            Write-Host "appName:      $($vppApp.displayName)"
            Write-Host "vppAppId:     $($vppApp.id)"
            Write-Host -ForegroundColor Blue "#####################################################"

            # Wait for input to continue or wait defined number of seconds and continue
            if ( $WaitDevice -lt 0 )
            {
                Write-Host ""
                Invoke-Pause -Message "Press any key to continue..."
                Write-Host ""
            } else {
                Write-Host ""
                Start-Sleep -Seconds $WaitDevice
            }
        }

        # If the flag was given, then actually go ahead and revoke the device license
        #
        # Reminder: this link has the info about the API. HOWEVER, it has some
        #   incorrect information about the formation of the link and the JSON
        #   body of the POST:
        #
        # https://docs.microsoft.com/en-us/graph/api/intune-apps-iosvppapp-revokedevicelicense?view=graph-rest-beta
        #
        # The link, as shown, lacks the `/microsoft.graph.iosVppApp` portion,
        #   and it doesn't work without it.
        #
        if ( $TakeAction )
        {
            # Construct body of call
            $body = @{
                managedDeviceId = "$($device.deviceId)"
                notifyManagedDevices = $false
            }

            # Turn that object into JSON
            $jsonBody = ConvertTo-Json $body

            # Construct URI for API call
            $uri = $baseAppUri + $vppApp.id + "/microsoft.graph.iosVppApp/revokeDeviceLicense"

            # Make API call to revoke the app license
            #
            # The try/catch will automatically try again if the call fails.
            # If the call fails twice, it'll try renewing the auth tokens.
            # If the call fails five times, it'll skip it.
            $tryCount = 0
            while ( $tryCount -lt 5 )
            {
                try
                {
                    Invoke-RestMethod -Uri $uri -Headers $authToken -Method Post -Body $jsonBody -ContentType "application/json" | Out-Null
                    $tryCount = 5
                }
                catch
                {
                    $tryCount++
                    $deviceList = $null

                    if ($Commentary) { Write-Host -ForegroundColor Yellow "API Failure: $tryCount" }

                    # If it has tried more than once, renew auth tokens; they're probably expired
                    if ( $tryCount -gt 1 )
                    {
                        # Renew tokens
                        if ($Commentary) { Write-Host -ForegroundColor Yellow "Renewing auth tokens" }
                        Connect-MSGraph
                        $authToken = & "$PSScriptRoot\Get-AuthToken.ps1" -User $APIUsername
                    }

                    Start-Sleep -Seconds 5
                }
            }


            if ( $Commentary ) { Write-Host -ForegroundColor Red "Revoked app license"}

            # Then wait for a little bit to give things time to settle
            Start-Sleep -Seconds $WaitSync

            ####################################################################
            # Sync the iPad so it uninstalls the app or placeholder and then
            #   reinstalls it.
            #
            # The try/catch will automatically try again if the call fails.
            # If the call fails twice, it'll try renewing the auth tokens.
            # If the call fails five times, it'll skip it.
            $tryCount = 0
            while ( $tryCount -lt 5 )
            {
                try
                {
                    Invoke-IntuneManagedDeviceSyncDevice -managedDeviceId $device.deviceId | Out-Null
                    $tryCount = 5
                }
                catch
                {
                    $tryCount++
                    $deviceList = $null

                    if ($Commentary) { Write-Host -ForegroundColor Yellow "API Failure: $tryCount" }

                    # If it has tried more than once, renew auth tokens; they're probably expired
                    if ( $tryCount -gt 1 )
                    {
                        # Renew tokens
                        if ($Commentary) { Write-Host -ForegroundColor Yellow "Renewing auth tokens" }
                        Connect-MSGraph
                        $authToken = & "$PSScriptRoot\Get-AuthToken.ps1" -User $APIUsername
                    }

                    Start-Sleep -Seconds 5
                }
            }
            ####################################################################

            if ( $Commentary ) { Write-Host -ForegroundColor Green "Syncing device"; Write-Host ""; Write-Host ""}
        }
    }
    ############################################################################
}

if ( $Commentary )
{
    Write-Host -ForegroundColor Red "Failed App Count:    $($vppFailedAppList.value.Count)"
    Write-Host -ForegroundColor Red "Failed Device Count: $deviceCount"
    Write-Host ""
    Write-Host ""
}
