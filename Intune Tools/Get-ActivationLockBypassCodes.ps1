<#
This script will take a JSON file exported from cfgutil, and it will export a
  list of activation lock bypass codes. These codes can be used to remove
  activation lock from an iPad using the Finder on macOS or iTunes on Windows.

More information about activation lock bypass can be found here:
  https://docs.microsoft.com/en-us/mem/intune/remote-actions/device-activation-lock-disable
#>

param(
    # Path to a JSON export from cfgutil.
    [string]$FilePath,

    # Username to use to authenticate with the Intune API
    [string]$APIUsername

)

$baseUri = "https://graph.microsoft.com/beta/deviceManagement/managedDevices"


# We'll be using Intune PowerShell SDK, so we need to authenticate for that
Connect-MSGraph

# Get an auth token for manual API calls
$authToken = & "$PSScriptRoot\Get-AuthToken.ps1" -User $APIUsername


# Get device list from Intune
$IntuneDevices = Get-IntuneManagedDevice

# Import data from JSON export from Configurator
$iPadJson = Get-Content $FilePath
$iPadInfo = $( $iPadJson | ConvertFrom-Json )

# Get all the properties of $iPadInfo.Output and then iterate over them
# Using hidden PSObject property to get to those properties as objects unto themselves
foreach ($property in $iPadInfo.Output.PSObject.Properties)
{
    if ( $property.Name -ne "Errors" )
    { # We don't want to get the error output that cfgutil puts in there at a random spot
        $iPadList += ,$property.Value.serialNumber
    }
}


foreach ( $serialNumber in $iPadList )
{
    # Lookkup device based on serial number
    $Device = $( $IntuneDevices | Where-Object serialNumber -eq "$serialNumber" )

    # Construct URL for Intune API request using the device's ID in AzureAD
    # MUST specifically include `$select=activationLockBypassCode` or that
    #   piece of information won't be returned; the property in the returned
    #   object will just be null.
    $uri="$baseUri/$($Device.id)?`$select=activationLockBypassCode"


    # Make the request and save the bypass code
    $bypassCode = $(Invoke-RestMethod -Uri $uri -Headers $authToken -Method Get).activationLockBypassCode

    # Construct properties for the new object
    $newDeviceEntry = [PSCustomObject]@{
        Name                     = $Device.deviceName
        SerialNumber             = $Device.serialNumber
        ActivationLockBypassCode = $bypassCode
    }

    # Create the new object and add it to an array of these objects
    $bypassList += ,$newDeviceEntry
}


$originalFileInfo = Get-ChildItem $FilePath

$exportFilePath = "$($originalFileInfo.DirectoryName)\$($originalFileInfo.BaseName).csv"

$bypassList | Export-Csv -Path $exportFilePath -NoTypeInformation -Encoding utf8
