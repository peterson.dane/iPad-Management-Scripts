<#
This is a *very* quick and dirty script. I used recursion, and there is
currently *no detection whatsoever for infinite recursion*!

Because of the recursive nature of the script, it *must* currently be called
from within the directory it's located in, otherwise it won't be able to call
itself.

TODO: At a minimum, the recursion should be moved inside a function inside the
      script, so that this isn't the case.

However, this should probably actually be restructured wholesale into something
that's loop-based. If not that, then at a(nother) minimum, there needs to be
some method for detecting circuits in the directed graph that would represent
our group membership heirarchy.

This isn't a problem for us, as I've made sure there are no such circuits, but
it's hanging over our heads like the sword of Damocles.

TODO: Get rid of the recursion. Or at least ad loop prevention to it.
      Maybe just pass a list of already processed IDs for groups?
#>

[cmdletbinding()]

param(
    # This is the GUID for the group we want to get the members of. A list of
    #   these can be obtained using the Intune PowerShell SDK, or you can get
    #   just the specific one you want in the MEM Portal.
    [string]$GroupId
)

# We'll be using Intune PowerShell Graph SDK, so we need to authenticate for that
Connect-MSGraph | Out-Null

# Get the group members
$groupMembers = Get-AADGroupMember -GroupId $groupId

# Sort out Devices and Groups
$devices = $groupMembers | Where-Object '@odata.type' -eq "#microsoft.graph.device"
$groups = $groupMembers | Where-Object '@odata.type' -eq "#microsoft.graph.group"

# If there are groups that are members of this group, keep digging down
if ( $groups.Length -gt 0 )
{
    foreach ( $group in $groups )
    {
        # Get the list of devices in any subgroup
        #
        # I know, I'm being bad an using recusion here with no mechanism to
        #   prevent endless recursion, but I know that a directed graph of our
        #   AAD group structure won't contain any loops, as things are.
        #
        # It works for now, and I need it to work now.
        $additionalDevices = .\Get-AllDeviceMembersOfAADGroup.ps1 -GroupId $group.id

        #Eliminate duplicates and add each of the new devices to the list
        Foreach ( $deviceToAdd in $additionalDevices )
        {
            # If the current device list doesn't contain a matching device, then add it to the array
            if ( !($devices.id -contains $deviceToAdd.id) )
            {
                $devices += ,$deviceToAdd
            }
        }
    }
}

return $devices
