# Notes on `Get-iPadEnrollmentProfileMobileconfig.ps1`

## Notes recorded while developing script
All the features being used in this script are only in the beta API, but it would
be good to use the Intune PowerShell SDK once they land in the stable version
of the API.

https://github.com/microsoft/Intune-PowerShell-SDK

--------------------

See this URL for info on the API call we're using:
https://docs.microsoft.com/en-us/graph/api/intune-enrollment-enrollmentprofile-exportmobileconfig?view=graph-rest-beta

--------------------

We don't currently have DEP set up for our iPads, and the API call that
grabs the enrollment profile requires a GUID for depOnboardingSettings in the
URI. However, I think it's possible that the Configurator enrollment creates
a "fake" depOnboardingSettings GUID.

Look at this URI from the MEM Portal:
`https://endpoint.microsoft.com/#blade/Microsoft_Intune_Enrollment/AppleConfiguratorProfileResourceBlade/overview/profileId/«GUID1»_«GUID2»`

I'm thinking that `GUID1` might be the DEP GUID. It's the same for all the
Configurator enrollment profiles. `GUID2` changes from profile to profile.

TESTING HAS NOW SHOWN THAT I AM 100% CORRECT ABOUT THIS, at least with
regards to the DEP GUID.

However, the Profile ID is actually the full thing: both GUIDs with the
underscore in between. This is also apparent when listing all the enrollment
profiles using an API call.

Perhaps this would be different for a real DEP profile than it is for
one created for Configurator-based enrollment?

--------------------

Microsoft, being Microsoft, doesn't actually say this in their API documentation,
but I pretty quickly figured out that the `.value` property returned on the
API call is the .mobileconfig file encoded in base64.

I initially had luck turning it into (mostly) human-readable UTF8, and this
looked essentially identical to manually-downloaded profiles, but when I
saved that UTF8 string to a file and tried to re-enroll my iPad, it didn't
work.

I was vexed at first, but I realized that I probably shouldn't have been
converting to a string using UTF8, but just to raw binary and saving that as
the .mobileconfig file. That *did* work.

--------------------
