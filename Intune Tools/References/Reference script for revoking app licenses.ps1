<#
This script was shared without restrictions by the author.

It can be found here:
    https://www.codepile.net/pile/L7K6x5Wd

More information can be found here:
    https://www.reddit.com/r/Intune/comments/ecuk5x/intune_vpp_apps_not_updating_no_licenses_being/fe9ch4f/


IMPORTANT:
The URIs used in this script actually contradicted the URIs specified in the
Intune API documentation — but they worked, and the ones from the official
docs didn't.

#>

#   If you want the script to gather all App Id's, uncomment section below
#   Get Id of all iOS apps that have failedDeviceCount greater than 0
$apiUrl1 = "https://graph.microsoft.com/beta/deviceAppManagement/mobileApps?filter=installSummary/failedDeviceCount gt 0 and isof('microsoft.graph.iosVppApp')&expand=installSummary&select=id"
$rest1 = Invoke-RestMethod -Headers @{Authorization = "Bearer $($Tokenresponse.access_token)"} -Uri $apiUrl1 -Method Get

#   If you uncommented the above section, uncomment line 8 and comment line 9.
#   If you want to just do one app at a time, put the App Id in the variable below.
#   Create array of all app ids
$appIds = @($rest1.value.id) #### commented out for testing of single app ids
# $appIds = @("83310a6a-1e8b-4c1c-8c8f-abc55ce21c96")

#   clear array of all deviceIds that have failed installs (useful if running twice)
Clear-Variable -Name failedIds

#   Loop GET request using $appids array to get deviceIds of failed install and add those deviceIds to $failedIds array
foreach ($appId in $appIds) {
    $apiUrl2 = "https://graph.microsoft.com/beta/deviceAppManagement/mobileApps/"+$appId+"/deviceStatuses/?filter=(installState eq 'failed')"
    $rest2 = Invoke-RestMethod -Headers @{Authorization = "Bearer $($Tokenresponse.access_token)"} -Uri $apiUrl2 -Method Get
    $failedIds += @($rest2.value.deviceId)
    }

################################################################################
# This part right here was the key to getting this call to work when I needed to
#   do this.
################################################################################
#   Loop POST request using $failedIds array to revoke app license on device app install failed on
foreach ($failedId in $failedIds) {
    $apiUrl3 = "https://graph.microsoft.com/beta/deviceAppManagement/mobileApps/"+$appId+"/microsoft.graph.iosVppApp/revokeDeviceLicense"
    $body = @{
        managedDeviceId=$failedId
        notifyManagedDevices= $false
        }
    $json = ConvertTo-Json $body
    $rest3 = Invoke-RestMethod -Headers @{Authorization = "Bearer $($Tokenresponse.access_token)"} -Uri $apiUrl3 -Method Post -Body $json -ContentType 'application/json'
    }
################################################################################


#   Loop POST request using $failedIds array to sync devices
foreach ($failedId in $failedIds) {
    $apiUrl5 = "https://graph.microsoft.com/beta/deviceManagement/managedDevices/"+$failedId+"/syncDevice"
    $rest5 = Invoke-RestMethod -Headers @{Authorization = "Bearer $($Tokenresponse.access_token)"} -Uri $apiUrl5 -Method Post
    }

#   Loop Get request using $appIds that failed to return revokeLicenseActionResults.
#   Not really necessary. Reports back too fast and has several pending statuses.
#foreach ($appId in $appIds) {
#    $apiUrl4 = "https://graph.microsoft.com/beta/deviceAppManagement/mobileApps/"+$appid+"/?select=microsoft.graph.iosVppApp/revokeLicenseActionResults"
#    $rest4 = Invoke-RestMethod -Headers @{Authorization = "Bearer $($Tokenresponse.access_token)"} -Uri $apiUrl4 -Method Get
#    $rest4.revokeLicenseActionResults | select managedDeviceId, actionState
#    }
