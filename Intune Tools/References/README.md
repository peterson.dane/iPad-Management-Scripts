The files in the `References` directory are mostly from Microsoft's `powershell-intune-samples` [repository on GitHub](https://github.com/microsoftgraph/powershell-intune-samples), and they were under an MIT license.

A couple of these were used as reference for doing some other things, though most of the scripts in the `Intune Tools` directory primarily use the [Intune PowerShell SDK](https://github.com/microsoft/Intune-PowerShell-SDK).

A full list of the commands in the current version of the Intune PowerShell SDK, based on v.1 of Intune's API, can be found in `List of PowerShell Intune SDK commands.txt`. You can get the same list by running `Get-Command -Module Microsoft.Graph.Intune`, but it was convenient to have this saved separately and to have it accessible and searchable in my editor.
