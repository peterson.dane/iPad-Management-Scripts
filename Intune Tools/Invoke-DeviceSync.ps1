<#
Sends sync requests from Intune to a list of devices specified by serial number
or by a JSON export from `cfgutil`.

TODO: All methods of device selection should *add* to the list of devices to be
      synced, not override each other.

TODO: Add ability to select devices to sync based on group membership in Intune

TODO: Add ability to select devices to sync based on `Grade / Use` column from inventory
#>

[cmdletbinding()]

param(
    # Path to a JSON file listing devices to sync (exported from cfgutil)
    # Using this parameter will OVERRIDE the -SerialNumbers parameter, not add
    #   to it.
    [string]$JsonPath,

    # Array of serial number strings of devices to sync
    # This parameter is OVERRIDEN by the -JsonPath parameter. If both are passed
    #   this parameter will be ignored.
    [string[]]$SerialNumbers
)


# We'll be using Intune PowerShell Graph SDK, so we need to authenticate for that
Connect-MSGraph | Out-Null

# Get device list from Intune
$IntuneDevices = Get-IntuneManagedDevice

# If a path to a JSON file has been provided, use that
if ( ![string]::IsNullOrWhiteSpace($JsonPath) )
{
    # Import data from JSON export from Configurator
    $iPadJson = Get-Content $JsonPath
    $iPadInfo = $( $iPadJson | ConvertFrom-Json )

    # Get all the properties of $iPadInfo.Output and then iterate over them
    # Using hidden PSObject property to get to those properties as objects unto themselves
    foreach ($property in $iPadInfo.Output.PSObject.Properties)
    {
        if ( $property.Name -ne "Errors" )
        { # We don't want to get the error output that cfgutil puts in there at a random spot
            $iPadList += ,$property.Value.serialNumber
        }
    }
# Otherwise, look for a string of Serial numbers and use those
} elseif ( ![string]::IsNullOrWhiteSpace($SerialNumbers) ) {
    $iPadList = $SerialNumbers
}

foreach ( $serialNumber in $iPadList )
{
    # Lookkup device based on serial number
    Write-Verbose "Syncing: $serialNumber"
    $Device = $( $IntuneDevices | Where-Object serialNumber -eq "$serialNumber" )

    Write-Verbose "$($Device.deviceName)"

    Invoke-IntuneManagedDeviceSyncDevice -managedDeviceId $Device.id

}
