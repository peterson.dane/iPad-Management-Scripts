<#
This script is pretty simple. It just grabs a list of our VPP apps, winnows the
list down to only the apps we've requested information on by BundleID, and then
exports the list as a CSV file.

It's mostly a quick way to check up on the number of licenses we have in total,
how many are assigned, and how many are left for each app.

Documentation for the API call we're using can be found here:
    https://docs.microsoft.com/en-us/graph/api/intune-apps-iosvppapp-list?view=graph-rest-1.0

TODO: Either ditch the file or add an additional parameter to just pass an array
      of strings consisting of BundleIDs.
#>

param(
    # File with a list of return-separated BundleIDs
    [string]$BundleIdFile

    # Username to use to authenticate with the Intune API
    [string]$APIUsername
)

$baseUri = "https://graph.microsoft.com/beta"
$deviceBaseUri = "https://graph.microsoft.com/beta/deviceManagement/managedDevices"

# Actually, we won't be using this, but I'm leaving the lines here in case we
#   eventually do. Maybe v.2 will have this functionality. If it does, it's
#   probably worth changing to be more internally-consistent.

# Leaving this here for whenever this functionality comes out of beta and makes
#   its way into the SDK
# Connect-MSGraph

# Get an auth token for manually-constructed API calls
$authToken = & "$PSScriptRoot\Get-AuthToken.ps1" -User $APIUsername

Write-Host ""

# Get list of BundleIDs for apps
$bundleIdList = Get-Content -Path "$bundleIdFile"

# Retrieve VPP App List
$uri = "$baseUri/deviceAppManagement/mobileApps"
$vppAppList = Invoke-RestMethod -Uri $uri -Headers $authToken -Method Get


# Get the subset of Apps that are in our BundleID-defined list
foreach ( $app in $vppAppList.value )
{
    if ( $bundleIdList.Contains($app.bundleId) )
    {
        $appListSubset += ,$app
    }
}

# Export list as CSV
$appListSubset | Export-Csv -Path "U:\Downloads\$(Get-Date -Format "yyyy-MM-dd_HH.mm")_AppList.csv" `
    -Encoding utf8 -NoTypeInformation
