<#
This scripts checks on the currency of iOS version on iPads, then email owners
if their iOS is outdated.

The script can take in an arry of iPad serial numbers, a path to a return-separated
list of iPad serial numbers, or an array of `Grade / Use` values from a CSV copy
of an iPad inventory. Providing a file will override an array of serials, and
providing an array of `Grade / Use` values will override both the other parameters.

The script then compares the current iOS version on all those iPads to the
-MinimumiOSVersion property. If the former is lower than the latter, it will
send an email to that person prompting them to update their iPad.

The script uses AD to look up email address. Because of this, the iPad inventory
needs to have `$First + " " + $(Last / Location)` match the displayName property
for the user in AD. (This should be the case anyway. We should keep these things
consistent across our directory and documentation.)

An HTML file that can be used for the body of the message can be found in this
same directory at `Send-iOSUpdateNotification.html`. You should replace some
commented-out placeholders before using it. (The script defaults to using this
file.)

There are a few find & replace actions done on the HTML file:

- FIRST_NAME will be replaced with the device owner's first name from the inventory
- IOS_VERSION will be replaced with the device's current installed version number
- IOS_CURRENT will be replaced with the value from the `MinimumiOSVersion` param

################################################################################

/!\ IF YOU HAVE NEW IPAD AIR UNITS, YOU WILL NEED TO MODIFY THIS SCRIPT
    It carves out an exemption based on that model name, but that will likely
    also catch newer iPad Airs, exempting them from notifications.

    Search the script for $iPadAirMinimumiOSVersion, and change that constant
    value to match the current version of iOS.

    There are some additional notes about this right above where that's set.

################################################################################

All the email functionality in this script was originally copied from
`Check-BackupHealth.ps1` in my `Scripts` repository.

TODO: Should really start looking at turning some of these pieces that I reuse
      into a module.
#>

[cmdletbinding()]

param
(
    # Show testing output; like -Verbose, but it doesn't trigger that behavior
    #   for everything.
    [switch]$Commentary,

    # Control whether to send email or just do a dry run
    [switch]$SendEmail,

    # List of serial numbers; all devices not on the list will be ignored.
    [string[]]$SerialList = $null,

    # Same as $SerialList, but points to a file of return-separated serials
    # THIS PARAMETER WILL OVERRIDE AND OVERWRITE $SerialList IF BOTH ARE PROVIDED
    #   File must be UTF8
    [string]$SerialPath = $null,

    # Filter iPad list based on the `Grade / Use` property from inventory.
    # Accepts array of strings, so you can include multiple values.
    # THIS PARAMETER WILL OVERRIDE BOTH OTHER FILTERING PARAMETERS, $SerialList and $SerialPath
    [string[]]$FilterUse = $null,

    # Location of Inventory document to link iPads to users
    #   File must be UTF8
    #   Must Contain the following columns: First,Last / Location,Serial Number
    [Parameter(Mandatory=$True)][string]$InventoryPath,

    # Minimum version of iOS that should be installed
    [Parameter(Mandatory=$True)][string]$MinimumiOSVersion,

    # Username to use to authenticate with the Intune API
    [string]$APIUsername,

    ############################################################################
    # Email Parameters

    # Location of HTML file for body of email.
    # Defaults to the file in the same path as the script.
    [string]$EmailBodyPath = "$PSScriptRoot\Send-iOSUpdateNotification.html",

    # Address to send from
    [string]$EmailAddress,

    # Server Information to send Email
    [string]$SmtpServer,
    [string]$SmtpPort,

    # Email password previously exported from SecureString object to encrypted file
    # Sample command to get and store password:
    #   Read-Host "Enter password" -AsSecureString | ConvertFrom-SecureString | Out-File "X:\file\path"
    [string]$EmailPasswordFile
)

################################################################################
# CONSTANTS
################################################################################

# This is the current latest OS version for the original iPad Air.
#   For us "iPad Air" is enough to differentiate the model from our other models.
#   If you have newer iPad Air models...uhhh...sorry, I guess, since you can't
#   check the A#### type model identifier in Intune.
#
#   Nice job on that device naming, Apple, and nice job on the data collection,
#   Microsoft.

$iPadAirMinimumiOSVersion = "12.4.6"
################################################################################


################################################################################
# FUNCTION: Invoke-Pause
#################################################################################
# Similar to cmd.exe `pause`, wait for any key to be pressed to continue
function Invoke-Pause {
    # Default to empty message string
    param( [string]$Message = "" )

    # Write message
    Write-Host -ForegroundColor Yellow "$Message"

    # Wait for input to continue
    $null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown')
}
################################################################################




################################################################################
# FUNCTION: Write-Commentary
################################################################################
# Will write commentary if flag is set; otherwise, it does nothing
# Just a light wrapper for Write-Host with some logic so I don't have to keep
#   testing this variable and writing repetitive code.
function Write-Commentary {
    param(
        # Message to be printed
        [string]$Message = "",

        # Just passes to the `Write-Host` parameter `ForegroundColor`
        [string]$ForegroundColor = "White"
    )

    if ( $Commentary )
    {
        Write-Host -ForegroundColor $ForegroundColor -Object $Message
    }
}

################################################################################




################################################################################
# FUNCTION: Send-iOSNotification
################################################################################
# This handles the actual business of sending an iOS notification through Intune
# Notifications only work with the company portal app installed and logged-in,
#   so I'm leaving this unimplemented for now, since we don't use it that way yet.
#
# TODO: Implement this feature.
#
function Send-iOSNotification {
    param(
        # microsoft.graph.managedDevice object from Intune/Graph
        [object]$Device
    )

    New-IosNotificationSettingsObject
}
################################################################################




################################################################################
# FUNCTION: Send-Email
################################################################################
# This will send the user an email
function Send-Email {
    param(
        # microsoft.graph.managedDevice object from Intune/Graph
        [object]$DeviceData,

        # One entry from our inventory
        [object]$InventoryData,

        # SmtpClient object with its properties all set up
        [object]$SmtpSetup,

        # Email address of sender
        [string]$SenderAddress
    )

    # Compose the name, since we'll be using it a few times
    $iPadOwner = "$($InventoryData.First) $($InventoryData."Last / Location")"


    # Look up info in AD
    $adEntry = Get-ADUser -Filter "displayName -eq ""$iPadOwner""" -Properties *

    # If AD returns nothing, give an error, otherwise keep going
    if ($adEntry -eq $null)
    {
        Write-Error "Could not find AD entry for $iPadOwner"
    } else {
        Write-Commentary -ForegroundColor Cyan -Message "Sending email to $iPadOwner"

        # Set up email subject
        $emailSubject = "Please Update Your iPad"

        # TODO: Create email body

        # Get email template
        $messageText = Get-Content $EmailBodyPath -Raw

        # Substitue in the data for this iPad
        $messageText = $messageText.Replace("FIRST_NAME",$InventoryData.First)
        $messageText = $messageText.Replace("IOS_VERSION",$DeviceData.osVersion)
        $messageText = $messageText.Replace("IOS_CURRENT",$MinimumiOSVersion)

        # Construct email message object
        $emailMessage = New-Object System.Net.Mail.MailMessage( $SenderAddress , $adEntry.mail )
        $emailMessage.isbodyhtml = $True
        $emailMessage.Subject = $emailSubject
        $emailMessage.Body = $messageText

        # Send email
        $SMTPClient.Send( $emailMessage )
    }
}
################################################################################




################################################################################
# MAIN BODY OF SCRIPT
################################################################################

################################################################################
# Import inventory
# TODO: Probably should try-catch this
$iPadInventory = Import-Csv -Path $InventoryPath -Encoding utf8

################################################################################
# Get our list of serials for devices we want to include

# Get serial numbers from inventory, if a `Grade / Use` filter was provided
if ( $FilterUse -ne $null )
{
    # Clear $SerialList, in case it was set, too
    $SerialList = @()

    # If this iPad's `Grade / Use` property is in the filter list
    # then add the serial number to our list
    foreach ($iPad in $iPadInventory)
    {
        if ( $FilterUse -contains $iPad."Grade / Use" )
        {
            $SerialList += ,$iPad."Serial Number"
        }
    }

# Otherwise, import serial numbers from file, if a path was provided
} elseif ( !([string]::IsNullorWhiteSpace($SerialPath)) ) {
    # TODO: Probably should try-catch the file read
    $SerialList = Get-Content -Path $SerialPath -Encoding utf8
}

################################################################################
# Graph API Setup
# Graph REST API URLs
$baseUri = "https://graph.microsoft.com/beta"
$baseDeviceUri = $baseUri + "/deviceManagement/managedDevices"


# We'll be using Intune PowerShell Graph SDK, so we need to authenticate for that
Connect-MSGraph | Out-Null

# Get an auth token for manual Graph API calls
$authToken = & "$PSScriptRoot\Get-AuthToken.ps1" -User $APIUsername

# Print a separator if commentary is on
Write-Commentary -Message ""
Write-Commentary -ForegroundColor DarkGray -Message "################################################################################"
Write-Commentary -Message ""

################################################################################
# Email server setup
# Only needs to be done if we're sending email
if ( $SendEmail )
{
    # Get the email password as a SecureString
    $emailPassword = ( Get-Content $EmailPasswordFile | ConvertTo-SecureString )

    $SMTPClient = New-Object System.Net.Mail.SmtpClient( $SmtpServer , $SmtpPort )
    $SMTPClient.EnableSsl = $True
    $SMTPClient.Credentials = New-Object System.Net.NetworkCredential( $EmailAddress , $emailPassword );
}

################################################################################
# Retrieve Device List; retry on failure
$tryCount = 0
while ( $tryCount -lt 5 )
{
    try
    {
        $deviceList = Get-IntuneManagedDevice
        $tryCount = 5
    }
    catch
    {
        $tryCount++
        $deviceList = $null

        if ($Commentary) { Write-Host -ForegroundColor Yellow "API Failure on Get-IntuneManagedDevice: $tryCount" }

        Start-Sleep -Seconds 5
    }
}

Write-Commentary -Message ""

################################################################################
# Check version, send emails/notifications

# First, filter out anything that's not an iOS device, then filter out all
#   devices with a version string that matches the current version exactly
$deviceList = ( $deviceList | Where-Object operatingSystem -eq "iOS" | `
                Where-Object osVersion -ne "$MinimumiOSVersion")

# Now iterate throught the remaining devices
Foreach ($device in $deviceList)
{
    # If this device isn't in our list, skip it
    if ( !($SerialList -contains $device.serialNumber) ) { continue }

    # Look up the device in our inventory to get other user info
    $iPadInventoryInfo = ( $iPadInventory | Where-Object "Serial Number" -eq $device.serialNumber )

    # Special handling for iPad Airs. This won't work if we ever get new iPad
    #   Airs, because they've done the dumb thing where they don't put a number
    #   in the model string, yet again. But the old Airs should be gone summer 2020.
    if ( $device.model -eq "iPad Air" )
    {
        # Test OS version, if it's greater than or equal to  min version, skip it
        if ( [version]$device.osVersion -ge [version]$iPadAirMinimumiOSVersion ) { continue }

        Write-Commentary -ForegroundColor Red -Message "iOS out of date on iPad: $($device.deviceName)"

        # Easier to not use Write-Commentary, here, as Format-List does this better
        if ( $Commentary ) { $device | Format-List -Property serialNumber,osVersion,model }

        # Send the email
        if ( $SendEmail )
        {
            Write-Commentary -ForegroundColor Cyan -Message "Sending email"

            Send-Email -DeviceData $device -InventoryData $iPadInventoryInfo `
                       -SmtpSetup $SMTPClient -SenderAddress $EmailAddress
        }

    } else { # This handling is for all other iPads
        # Test to be sure that OS version isn't greater or equal our min version
        #   (only filtered out exact matches earlier)
        # If it its greater than or equal to our minimum version, skip this device
        if ( [version]$device.osVersion -ge [version]$MinimumiOSVersion ) { continue }

        Write-Commentary -ForegroundColor Red -Message "iOS out of date on iPad: $($device.deviceName)"

        # Easier to not use Write-Commentary, here, as Format-List does this better
        if ( $Commentary ) { $device | Format-List -Property serialNumber,osVersion,model }


        if ( $SendEmail )
        {
            Write-Commentary -ForegroundColor Cyan -Message "Sending email"

            Send-Email -DeviceData $device -InventoryData $iPadInventoryInfo `
                       -SmtpSetup $SMTPClient -SenderAddress $EmailAddress
        }

        # TODO: Implement this feature
        # if ( $SendNotification )
        # {
        #     Write-Commentary -ForegroundColor Cyan -Message "Sending iOS notification"
        # }
    }

    Write-Commentary -Message ""
    Write-Commentary -ForegroundColor DarkGray -Message "################################################################################"
    Write-Commentary -Message ""

}
