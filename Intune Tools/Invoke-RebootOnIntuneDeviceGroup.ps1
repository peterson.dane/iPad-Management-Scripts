<#
Tell a group of iPads to all reboot. This only works on iPads that are supervised,
either through Configurator or DEP.

#>

[cmdletbinding()]

param(
    [string[]]$GroupIds
)

# We'll be using Intune PowerShell Graph SDK, so we need to authenticate for that
Connect-MSGraph | Out-Null

foreach ( $groupId in $GroupIds )
{
    $groupMembers = .\Get-AllDeviceMembersOfAADGroup.ps1 -GroupId $groupId

    foreach ( $device in $groupMembers )
    {
        Write-Host -ForegroundColor Cyan "Rebooting $($device.displayName):"
        Write-Host -ForegroundColor Gray "$($device.id)"

        # Send the reboot command
        Invoke-IntuneManagedDeviceRebootNow -managedDeviceId $device.deviceId

        Write-Host ""
    }
}
