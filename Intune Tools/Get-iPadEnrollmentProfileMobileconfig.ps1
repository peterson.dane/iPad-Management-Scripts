<#
This script will download a copy of a .mobileconfig enrollment profile for
manual re-enrollment of an iPad that has been removed from the MDM.

The GUID for the DEP ID and the "double-GUID" of the Profile ID must be
specified. You can get these using the API if you're using the DEP. See the
markdown file with the same name as this script for more information.
#>


param(
    # Output .mobileconfig profile path
    [string]$OutFile,

    # DEP GUID for the enrollment profile we want to output
    #   Could set this to default to the GUID most (or all) of your profiles are
    #   associated with by adding ` = ""` at the end of the line before the comma.
    [string]$DepId,

    # Full ID for the enrollment profile we want to download
    [string]$ProfileId,

    # Username to use to authenticate with the Intune API
    [string]$APIUsername
)

$baseUri = "https://graph.microsoft.com/beta"
$depBaseUri = "$baseUri/deviceManagement/depOnboardingSettings/$DepId"


# Get an auth token for manually-constructed API calls
$authToken = & "$PSScriptRoot\Get-AuthToken.ps1" -User $APIUsername


# Construct URL to get the mobileconfig profile
$uri = "$depBaseUri/enrollmentProfiles/$ProfileId/exportMobileConfig"

# Make the API call
$apiReturn = Invoke-RestMethod -Uri $uri -Headers $authToken -Method Get

# Turn the base64 into bytes and output that to the file location.
$bytes = [Convert]::FromBase64String($apiReturn.value)
[IO.File]::WriteAllBytes($OutFile,$bytes)
