/** @OnlyCurrentDoc */

/*
Macros to automatically apply formatting to the CSV file exported by
Invoke-DeviceEnrollmentCheckup.ps1 after it is opened in Google Sheets.

These were (mostly) recorded using the macro recorder in Google Sheets.

After they're imported into the sheet, you can invoke all the formatting using
the `IntuneEnrollmentCheckupFormatting` macro.

Due to some limitations, the conditional formatting for the `Needs Attention`
column must be edited manually after the macros are run.
*/

function IntuneEnrollmentCheckupFormatting() {
  var spreadsheet = SpreadsheetApp.getActive();
  spreadsheet.getRange('1:1').activate();
  spreadsheet.getActiveRangeList().setFontWeight('bold');
  var sheet = spreadsheet.getActiveSheet();
  sheet.getRange(1, 1, sheet.getMaxRows(), sheet.getMaxColumns()).activate();
  spreadsheet.getActiveRangeList().setFontFamily('Roboto');
  spreadsheet.getRange('I:L').activate();
  spreadsheet.getActiveRangeList().setFontFamily('Roboto Mono');
  spreadsheet.getRange('I1:L1').activate();
  spreadsheet.getActiveRangeList().setFontFamily('Roboto');
  sheet = spreadsheet.getActiveSheet();
  sheet.getRange(1, 1, sheet.getMaxRows(), sheet.getMaxColumns()).activate();
  spreadsheet.getActiveSheet().autoResizeColumns(1, 26);
  spreadsheet.getRangeList(['B:B', 'D:D', 'E:E', 'F:F']).activate()
  .setHorizontalAlignment('left');
  spreadsheet.getRange('D:D').activate();
  spreadsheet.getActiveRangeList().setNumberFormat('yyyy"-"mm"-"dd" "hh":"mm":"ss');
  spreadsheet.getRange('C2:C1000').activate();
  spreadsheet.getRange('C2:C1000').setDataValidation(SpreadsheetApp.newDataValidation()
  .setAllowInvalid(true)
  .requireCheckbox()
  .build());
  spreadsheet.getRange('E:E').activate();
  ApplyFilter();
  ApplyColorscale();
  ApplyHighlight();
};

function ApplyFilter() {
  var spreadsheet = SpreadsheetApp.getActive();
  var sheet = spreadsheet.getActiveSheet();
  sheet.getRange(1, 1, sheet.getMaxRows(), sheet.getMaxColumns()).activate();
  sheet = spreadsheet.getActiveSheet();
  sheet.getRange(1, 1, sheet.getMaxRows(), sheet.getMaxColumns()).createFilter();
};

function ApplyColorscale() {
  var spreadsheet = SpreadsheetApp.getActive();
  spreadsheet.getRange('E2:E1000').activate();
  var conditionalFormatRules = spreadsheet.getActiveSheet().getConditionalFormatRules();
  conditionalFormatRules.push(SpreadsheetApp.newConditionalFormatRule()
  .setRanges([spreadsheet.getRange('E2:E1000')])
  .whenCellNotEmpty()
  .setBackground('#B7E1CD')
  .build());
  spreadsheet.getActiveSheet().setConditionalFormatRules(conditionalFormatRules);
  conditionalFormatRules = spreadsheet.getActiveSheet().getConditionalFormatRules();
  conditionalFormatRules.splice(conditionalFormatRules.length - 1, 1, SpreadsheetApp.newConditionalFormatRule()
  .setRanges([spreadsheet.getRange('E2:E1000')])
  .setGradientMinpoint('#57BB8A')
  .setGradientMaxpoint('#FFFFFF')
  .build());
  spreadsheet.getActiveSheet().setConditionalFormatRules(conditionalFormatRules);
  conditionalFormatRules = spreadsheet.getActiveSheet().getConditionalFormatRules();
  conditionalFormatRules.splice(conditionalFormatRules.length - 1, 1, SpreadsheetApp.newConditionalFormatRule()
  .setRanges([spreadsheet.getRange('E2:E1000')])
  .setGradientMinpoint('#6D9EEB')
  .setGradientMaxpoint('#FFFFFF')
  .build());
  spreadsheet.getActiveSheet().setConditionalFormatRules(conditionalFormatRules);
  conditionalFormatRules = spreadsheet.getActiveSheet().getConditionalFormatRules();
  conditionalFormatRules.splice(conditionalFormatRules.length - 1, 1, SpreadsheetApp.newConditionalFormatRule()
  .setRanges([spreadsheet.getRange('E2:E1000')])
  .setGradientMinpoint('#6D9EEB')
  .setGradientMaxpoint('#F6B26B')
  .build());
  spreadsheet.getActiveSheet().setConditionalFormatRules(conditionalFormatRules);
};

function ApplyHighlight() {
  var spreadsheet = SpreadsheetApp.getActive();
  spreadsheet.getRange('C2:C1000').activate();
  var conditionalFormatRules = spreadsheet.getActiveSheet().getConditionalFormatRules();
  conditionalFormatRules.push(SpreadsheetApp.newConditionalFormatRule()
  .setRanges([spreadsheet.getRange('C2:C1000')])
  .whenCellNotEmpty()
  .setBackground('#F4C7C3')
  .build());
  spreadsheet.getActiveSheet().setConditionalFormatRules(conditionalFormatRules);
  conditionalFormatRules = spreadsheet.getActiveSheet().getConditionalFormatRules();
  // Boolean condition type not supported in Macro Recorder yet.
  spreadsheet.getActiveSheet().setConditionalFormatRules(conditionalFormatRules);
  conditionalFormatRules = spreadsheet.getActiveSheet().getConditionalFormatRules();
  // Boolean condition type not supported in Macro Recorder yet.
  spreadsheet.getActiveSheet().setConditionalFormatRules(conditionalFormatRules);
  conditionalFormatRules = spreadsheet.getActiveSheet().getConditionalFormatRules();
  // Boolean condition type not supported in Macro Recorder yet.
  spreadsheet.getActiveSheet().setConditionalFormatRules(conditionalFormatRules);
};
