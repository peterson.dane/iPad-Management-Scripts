<#
Given an array of BundleIDs and an array of GroupIDs for AAD groups, this
script will automatically assign all the apps to all the groups.

The `useDeviceLicensing` and `uninstallOnDeviceRemoval` properties are set to
true for the apps.

TODO: Control the above properties with parameters? (Not something needed in
      our setup, but it could certainly be useful for others --- or when/if we
      make changes.)

TODO: Intune has a new property on the app assignments: `Install as removable`
      Figure out what the property name is for this in the API objects. Then
      get that set to TRUE. It seems to default to TRUE, but we should
      explicitly set it, too. We *do* want people to be able to remove these
      apps when we assign them, as that can be an important step in
      troubleshooting.

      Also consider adding a parameter to set this property. (See other TODO)

################################################################################

I found this script that demonstrated setting the "Uninstall on device removal"
property. It's not in the SDK, because it's newer than v1.0 of the API, which
the SDK is based on. But the call is present in the beta version of the API.

As I recall, it wasn't documented in even the beta version of the API when I was
working on this, though.

https://github.com/MSEndpointMgr/Intune/blob/master/Apps/Set-IntuneiOSManagedAppAssignment.ps1

See `Set-IntuneiOSManagedAppAssignment.ps1` in `References` directory for the
script itself.

#>

[cmdletbinding()]
param (
    # Bundle IDs for apps to be deployed
    [string[]]$BundleIds,

    # Email address associated with the VPP token the app is associated with
    [string]$VppEmail,

    # Intune ID for groups to make assignment to
    [string[]]$GroupIds,

    # Username to use to authenticate with the Intune API
    [string]$APIUsername
)

################################################################################
# Graph API Setup
# Graph REST API URIs
$baseUri = "https://graph.microsoft.com/beta"
$baseAppUri = $baseUri + "/deviceAppManagement/mobileApps/"


# We'll be using Intune PowerShell Graph SDK, so we need to authenticate for that
Connect-MSGraph | Out-Null

# Get an auth token for manual Graph API calls
$authToken = & "$PSScriptRoot\Get-AuthToken.ps1" -User $APIUsername

################################################################################
# Get app list - only get VPP apps
$vppApps = Get-IntuneMobileApp -Filter "isof('microsoft.graph.iosVppApp')"

# Iterate through the Bundle IDs that were passed
foreach ( $bundleId in $BundleIds )
{
    $app = ( $vppApps | Where-Object bundleId -eq $bundleId )

    # If more than one result is returned, also filter by the VPP token email
    if ( $app.Count -gt 1 )
    {
        # Verify that $VppEmail isn't blank, otherwise give error and continue
        if ( [string]::IsNullorWhiteSpace($VppEmail) )
        {
            Write-Error "Multiple app listings were found for $bundleID, but -VppEmail was not provided. Skipping this app."
            Continue
        }

        $app = ( $app | Where-Object vppTokenAppleId -eq $VppEmail )
    }

    # If no results were found for this bundleId and/or vppTokenAppleId
    if ( $app.Count -eq 0 )
    {
        # Then give an error and continue the loop
        Write-Error "Could not find any entries for $bundleId"
        Continue
    }

    # Create app assignment settings
    #   https://docs.microsoft.com/en-us/graph/api/resources/intune-apps-iosVppAppAssignmentSettings?view=graph-rest-1.0
    # TODO: When SDK is updated, add the flag for `uninstallOnDeviceRemoval`
    $assignmentSettings = New-MobileAppAssignmentSettingsObject `
        -iosVppAppAssignmentSettings -useDeviceLicensing $true

    # Add a member to the $assignmentSettings to accomodate property added in 2019-10
    #   this isn't in the SDK yet. SDK is built on v.1, and this is only in beta
    # TODO: When SDK is updated, remove this line
    $assignmentSettings | Add-Member -MemberType NoteProperty -Name "uninstallOnDeviceRemoval" -Value $true

    # Now, for each bundle, iterate through all the passed groups, and do the
    #   app assignment for each
    foreach ( $groupId in $GroupIds )
    {
        # Create assignment target for group
        #   https://docs.microsoft.com/en-us/graph/api/resources/intune-shared-groupassignmenttarget?view=graph-rest-1.0
        $assignmentTarget = New-DeviceAndAppManagementAssignmentTargetObject `
            -groupAssignmentTarget -groupId $groupId

        ########################################################################
        # TOOK THIS SECTION OUT - SDK uses v.1 of the API, but we need beta
        #   in order to set the `uninstallOnDeviceRemoval` property
        # TODO: Once the SDK gets updated, go back to using the SDK command
        #       It's cleaner, simpler, easier to read
        ########################################################################
        # # Do the actual assignment
        # #   https://docs.microsoft.com/en-us/graph/api/intune-apps-mobileappassignment-create?view=graph-rest-1.0
        # New-IntuneMobileAppAssignment -mobileAppId $app.id -intent "required" `
        #     -settings $assignmentSettings -target $assignmentTarget
        ########################################################################

        # BETA API CALL ########################################################
        # Need to manually do the API call, since we need to use the beta API
        # Set up URI for the API call
        $uri = $baseAppUri + $app.id + "/assignments/"

        # Get the assignment object all set up
        $bodyTable = @{
            "@odata.type" = "#microsoft.graph.mobileAppAssignment"
            "intent" = "required"
            "target" = $assignmentTarget
            "settings" = $assignmentSettings
        }

        # Convert the assignment object to JSON for the API call
        $body = ( $bodyTable | ConvertTo-Json )

        # Make that call!
        Invoke-RestMethod -Uri $uri -Headers $authToken -Body $body -Method POST

        # END OF BETA CALL #####################################################
    }
}
