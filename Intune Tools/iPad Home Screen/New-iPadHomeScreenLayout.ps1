# This file will take a CSV file exported from `App List for Intune` document
#   and it will generate a homescreen config. It will then upload that config
#   and create it in Intune, allowing you to assign it to whatever groups you
#   want.
#
# TODO: Really need to create a way to define a custom sort for pages other than
#       lexicographic. This works for us now because they end up sorting
#       correctly, but that might not be the case if we start making more complex
#       configs or custom configs.
#
#       This shouldn't just be something in the `App List for Intune` doc,
#       because that would vastly overcomplicate that document and its maintenance.
#
#       Perhaps the easiest way would be a few little CSV files? (But how to
#       this extensible?) Maybe (an) object(s) saved to disk as a JSON would be
#       more extensible? Something else?
#
#       /!\ CSV could work if we don't have any duplicate folder names across
#           but that would mean an additional document to maintain for each
#           home screen config.
#
#       /!\ For ease of import XML or JSON would also be good candidates.
#
#
# TODO: At some point it might be nice to allow a way to define a custom sort
#       for folders within pages, too. Probably would bind that up with whatever
#       method gets used for defining custom sort for pages.
#


[cmdletbinding()]
param(
    # Location of the CSV file with homescreen information for apps
    # See `App List for Intune` in Tech shared drive for an example of how this should look
    [string]$FilePath,

    # Human-readable name for the new config profile; this can be any short
    #   descriptive string; there aren't requirements against using spaces or
    #   anything. It's intended to be human-readable.
    [string]$ProfileName,

    # Description of the profile.
    [string]$ProfileDescription
)


################################################################################
# FUNCTION: New-HomescreenPagePrecursor
################################################################################
# This will take any collection of apps for a particular homescreen, and it will
# return an array of `iosHomeScreenItem` objects.
#
# Don't want to just return an `iosHomeScreenPage` object, because then we
# couldn't use it to generate the list for the Dock, too. This way, all this
# otherwise identical code is totally reusable
################################################################################
function New-HomescreenPagePrecursor {
    param(
        # This takes in an array of all the app info objects from our app
        #   inventory that go on the page.
        [PSObject]$appListPage
    )

    # This will be the variable to hold the array of `iosHomeScreenItem` objects
    #   Don't technically  need to make it here, but I think it's clearer to
    #   define and explain it right upfront, rather than have its creation
    #   tucked away in a loop
    $page = @()

    # Modify the Order property to allow us to sort it correctly
    foreach ( $app in $appListPage )
    {
        if ( $app.Order -eq "x" )
        {   # If the orer property is `x`, change it to an int, 9999
            # This *should* save as an int even without casting, but just making sure.
            $app.Order = [int]9999

        } else {
            #Otherwise convert the current string value to an int
            $app.Order = [int]($app.Order)
        }
    }

    # Get the apps in the root level of the Dock, those where `Homescreen Folder`
    #   property is an empty string
    $appListPageRoot = $appListPage | Where-Object "Homescreen Folder" -eq ""

    # Sort the list first by `Order` then by `App Name`
    $appListPageRoot = $appListPageRoot | Sort-Object -Property "Order","App Name"

    # For each of these apps, create an `iosHomeScreenItem` and add it to our $dock array
    foreach ( $app in $appListPageRoot )
    {
        $page += ,$( New-IosHomeScreenItemObject -iosHomeScreenApp `
                        -bundleID $app."Bundle ID" -displayName $app."App Name" )
    }

    # Get a list of all subfolders in the page
    $folderList = $($appListPage | Where-Object "Homescreen Folder" -ne "")."Homescreen Folder" | Sort-Object | Get-Unique

    # Create each folder and add it to the $page
    foreach ( $folderName in $folderList )
    {
        # Filter down to just the items in this folder
        $appListFolder = $appListPage | Where-Object "Homescreen Folder" -eq "$folderName"


        # Sort the apps correctly
        $appListFolder = $appListFolder | Sort-Object -Property "Order","App Name"

        # $appListFolder | Format-List -Property "App Name",Order,"Homescreen Folder"

        # Clear out the $folder variable that will hold the array of
        #   `iosHomeScreenItem` objects for this folder
        $folder = @()

        # Add `iosHomeScreenItem` for each app to this folder's array
        foreach ( $app in $appListFolder )
        {
            $folder += ,$( New-IosHomeScreenItemObject -iosHomeScreenApp `
                                -bundleID $app."Bundle ID" `
                                -displayName $app."App Name" )
        }

        # Create `iosHomeScreenFolderPage` object
        $folderPage = New-IosHomeScreenFolderPageObject -apps $folder -displayName $folderName

        # Create an `iosHomeScreenFolder` object with one page - the one we just made
        #   and add that to $page
        $page += ,$( New-iosHomeScreenItemObject -iosHomeScreenFolder `
                        -displayName $folderName -pages $folderPage )
    }

    # $page array should be complete, now

    return $page
}
################################################################################
# END FUNCTION: New-HomescreenPagePrecursor
################################################################################


################################################################################
# Don't  strictly need to define either of these up here, but I want to to make
#   it clear what they are, rather than just creating and documenting them in
#   the middle of the script.

# This variable will hold the array of `iosHomeScreenItem` objs for the Dock
$dock = @()

# This variable will hold the array of `iosHomeScreenPage` objs for our homescreen config
$homescreen = @()
################################################################################

# We'll be using Intune PowerShell Graph SDK, so we need to authenticate for that
Connect-MSGraph | Out-Null

# Get the app list from the file
$appList = Import-Csv -Path $FilePath -Encoding utf8

# First get rid of all entries which are not to be installed, as they might have bad data
$appList = $appList | Where-Object "To Be Installed" -ne "FALSE"

# Just get rid of all entries with no `Homescreen Page`, as we won't touch them
$appList = $appList | Where-Object "Homescreen Page" -ne ""

################################################################################
# Get the apps assigned to the Dock
$appListDock = $appList | Where-Object "Homescreen Page" -eq "Dock"

# This variable will hold the array of `iosHomeScreenItem` objs for the Dock
$dock = New-HomescreenPagePrecursor -appListPage $appListDock

################################################################################
# Get all the apps that *aren't* assigned to the Dock
$appListNotDock = $appList | Where-Object "Homescreen Page" -ne "Dock"

# Get a list of all these other pages
$pageList = $appListNotDock."Homescreen Page" | Sort-Object | Get-Unique

foreach ( $pageName in $pageList )
{
    # Filter down to just the items on this page and subfolders
    $appListPagePrecursor = $appListNotDock | Where-Object "Homescreen Page" -eq "$pageName"

    # Generate the array of `iosHomeScreenItem` objects
    $pagePrecursor = New-HomescreenPagePrecursor -appListPage $appListPagePrecursor

    # Create the page and add it to the $homescreen array of pages
    $homescreen += ,$( New-IosHomeScreenPageObject -displayName $pageName `
                            -icons $pagePrecursor )
}

# Make the API call to create a config policy for this homescreen. We'll handle
#   the assignment manually from the MEM console.
New-IntuneDeviceConfigurationPolicy -iosDeviceFeaturesConfiguration `
    -homeScreenDockIcons $dock -homeScreenPages $homescreen `
    -displayName $ProfileName -description $ProfileDescription
