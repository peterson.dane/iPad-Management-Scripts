# Notes on using `New-iPadHomeScreenLayout.ps1`

⚠ **Important:**

> While Microsoft's documentation claims that each iosHomeScreenPage object can have up to 500 elements, this is not true in practice. 20 icons fit on one home screen, and while it will overflow those icons to a second screen, it will cut off after 40 icons, including apps and folders.
>
> While it would be nice for the script to handle this, for now, you need to handle it and keep track of things yourself by splitting them up between home screens manually. Because the home screen display name doesn't show up anywhere on the device, we can just precede names with a number in order to get them to sort the way we want.

### CSV App Inventory Input File
One of the inputs this script uses is a CSV exported from our `App List for Intune` document. This has the following properties for each app. Bolded properties are ones used in this script, and a description follows each about how it is used:

- **App Name** - *this is only visible within the MEM Portal; it's not user-facing. Apps are ordered on the screen by this property after being sorted by the `Order` property*

- Display Name

- **Bundle ID** - *this is the core property used to configure the home screen layout*

- TrackID

- Dock

- **Order** - *apps are ordered by this property first, then by app name. I would suggest numbering a bit like an old BASIC program: 10, then 20, then 30, etc., instead of 1, then 2, then 3, etc. This allows for insertion of new items and rearrangement of existing items without having to renumber everything for every change. If the app should just be part of the default alphabetical arrangement, listed after the specifically ordered apps, this column must have a value of `x` (lower case x).*

- **Homescreen Page** - *defines which home screen page the app should appear on. `Dock` is a valid option here, but it gets treated a bit differently by the script, as that's a separate property on the Intune PowerShell SDK cmdlet.*

- **Homescreen Folder** - *defines whether an app should appear within a folder on the home screen page; if this is blank, the app will show up on the page itself, not within a folder*

- Price

- AppStore URL

- VPP URL

- To Be Installed

- Home Screen Set

- App Assnd

- Removed from Store or VPP

- BUILTIN

- *In the actual spreadsheet there is along list of columns with checkboxes to indicate which groups are supposed to get a particular app installed. These are not reproduced here.*

- Install on Subgroup

- Notes



# Notes From Making `New-iPadHomeScreenLayout.ps1`

I took these while I was working on making this script, so it seemed best to keep them around.

This script was originally going to generate a JSON file which could be posted to the Intune API, but on closer inspection, it seems clear that all the commands needed to make this happen already existed within the Intune PowerShell SDK. It was just that the PowerShell commands didn't map 1:1 to the API documentation (lots of API calls that are documented separately are combined into single commands with the separate calls invoked using switch parameters).


## New-IntuneDeviceConfigurationPolicy

This is the way to create a new device config policy. There is no individual command that maps directly to the first API call linked below, but this seems to incorporate all the different types using various parameters.


- `-iosDeviceFeaturesConfiguration <SwitchParameter>` This just tells the command that this is a Device Features Config for iOS/iPadOS.

- `-homeScreenDockIcons <object[]>` This doesn't reall mean "icons" so much as it means "home screen objects" It takes an array of app and folder objects from `New-IosHomeScreenItemObject`.

- `-homeScreenPages <object[]>` This takes an array of `iosHomeScreenPage` objects from `New-IosHomeScreenPageObject`.

Links:
https://docs.microsoft.com/en-us/graph/api/intune-deviceconfig-iosdevicefeaturesconfiguration-create



## New-IosHomeScreenPageObject

Command to create the object for a new home screen page on iOS.

- `-displayName <string>`

- `-icons <object[]>` This doesn't really mean "icons" so much as it means "Home Screen Objects". It takes an array of objects from `New-IosHomeScreenItemObject`.

  Links:
  https://docs.microsoft.com/en-us/graph/api/resources/intune-deviceconfig-ioshomescreenpage



## New-IosHomeScreenItemObject

Creates a new app or folder for home screen.

- `-iosHomeScreenApp <SwitchParameter>`
  - `-bundleID <string>`
  - `-displayName <string>`

- `-iosHomeScreenFolder <SwitchParameter>`
  - `-displayName <string>`
  - `-pages <object[]>`

Links:
https://docs.microsoft.com/en-us/graph/api/resources/intune-deviceconfig-ioshomescreenitem
https://docs.microsoft.com/en-us/graph/api/resources/intune-deviceconfig-ioshomescreenapp
https://docs.microsoft.com/en-us/graph/api/resources/intune-deviceconfig-ioshomescreenfolder



## New-IosHomeScreenFolderPageObject

Creates a new page for an iOS home screen folder. Needed as input to create a folder object using New-IosHomeScreenItemObject

- `-apps <object[]>` Takes an array of apps from `New-IosHomeScreenItemObject`.
- `-displayName <string>` Just a display name for the folder page.


Links:
https://docs.microsoft.com/en-us/graph/api/resources/intune-deviceconfig-ioshomescreenfolderpage
