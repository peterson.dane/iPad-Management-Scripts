<#
Takes in an array of iPad DeviceIDs (from Intune), then takes several steps to
retire them.

TODO: Integrate this script into `Retire-IntuneManagediPad.ps1`, rather than
      having two separate and nearly identical scripts.

#>

# TODO: Just make the GET call for each device's info once, not in ever foreach loop!

param(
    # List of DeviceIDs of iPads to be retired
    [string[]]$iPadList,

    # Username to use to authenticate with the Intune API
    [string]$APIUsername,

    # Show only the text outputs
    [switch]$TestRun,

    # Wait in between steps
    [int]$Wait = 600
)

$baseUri = "https://graph.microsoft.com/beta"
$deviceBaseUri = "https://graph.microsoft.com/beta/deviceManagement/managedDevices"

# We'll be using Intune PowerShell Graph SDK, so we need to authenticate for that, too
Connect-MSGraph

# # Wait until they've done that!
# Write-Host 'Press any key to continue...';
# $null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');


# Get an auth token for manually-constructed API calls
$authToken = & "$PSScriptRoot\Get-AuthToken.ps1" -User $APIUsername

Write-Host ""

# Revoke licenses
#   https://docs.microsoft.com/en-us/graph/api/intune-devices-manageddevice-revokeapplevpplicenses?view=graph-rest-beta
foreach ( $deviceId in $iPadList )
{
    # Get device info based on ID
    $Device = Get-IntuneManagedDevice -managedDeviceId $deviceId

    Write-Host -ForegroundColor Yellow "Revoking VPP licenses for $($Device.deviceName)"
    Write-Host "$($Device.id)"
    Write-Host ""

    # Construct URL for Graph API request using the device's ID in AzureAD
    $uri="$deviceBaseUri/$($Device.id)/revokeAppleVppLicenses"

    if ( -not $TestRun)
    {
        # Make the API call
        Invoke-RestMethod -Uri $uri -Headers $authToken -Method Post
    } else {
        Start-Sleep -seconds 1
    }
}


# ACTUALLY, DON'T SYNC - according to my more recent reading of the documentation
#   syncing will cause the devices to re-pull active licnenses.
# Sync devices
# foreach ( $deviceId in $iPadList )
# {
#     # Get device info based on ID
#     $Device = Get-IntuneManagedDevice -managedDeviceId $deviceId
#
#     Write-Host -ForegroundColor DarkBlue "Syncing $($Device.deviceName)"
#     Write-Host "$($Device.id)"
#     Write-Host ""
#
#     if ( -not $TestRun)
#     {
#         # Make the API call
#         Invoke-IntuneManagedDeviceSyncDevice -managedDeviceId $Device.id
#     } else {
#         Start-Sleep -seconds 1
#     }
# }


if ( $Wait -gt 0 )
{
    Write-Host -foregroundcolor Blue "Waiting $Wait seconds for license revocation..."
    Start-Sleep -seconds $Wait
    Write-Host -foregroundcolor Blue "Moving to next step..."
    Write-Host ""
}


# Retire devices
foreach ( $deviceId in $iPadList )
{
    # Get device info based on ID
    $Device = Get-IntuneManagedDevice -managedDeviceId $deviceId

    Write-Host -ForegroundColor Red "Retiring $($Device.deviceName)"
    Write-Host "$($Device.id)"
    Write-Host ""


    if ( -not $TestRun)
    {
        # Make the API call
        Invoke-IntuneManagedDeviceRetire -managedDeviceId $Device.id
    } else {
        Start-Sleep -seconds 1
    }
}


# Sync devices again
foreach ( $deviceId in $iPadList )
{
    # Get device info based on ID
    $Device = Get-IntuneManagedDevice -managedDeviceId $deviceId

    Write-Host -ForegroundColor DarkBlue "Syncing $($Device.deviceName)"
    Write-Host "$($Device.id)"
    Write-Host ""

    if ( -not $TestRun)
    {
        # Make the API call
        Invoke-IntuneManagedDeviceSyncDevice -managedDeviceId $Device.id
    } else {
        Start-Sleep -seconds 1
    }
}


if ( $Wait -gt 0 )
{
    Write-Host -foregroundcolor Blue "Waiting $Wait seconds for sync..."
    Start-Sleep -seconds $Wait
    Write-Host -foregroundcolor Blue "Moving to next step..."
    Write-Host ""
}

Write-Host -ForegroundColor Yellow 'Check on devices in Intune. Then press any key to delete devices from Intune...';
$null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');

Write-Host -ForegroundColor Yellow 'Press any key once again to confirm deletion...';
$null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');

# DELETE devices from Intune
foreach ( $deviceId in $iPadList )
{
    # Get device info based on ID
    $Device = Get-IntuneManagedDevice -managedDeviceId $deviceId

    Write-Host -ForegroundColor Red "Deleting $($Device.deviceName)"
    Write-Host "$($Device.id)"
    Write-Host ""

    if ( -not $TestRun)
    {
        # Make the API call
        Remove-IntuneManagedDevice  -managedDeviceId $Device.id
    } else {
        Start-Sleep -seconds 1
    }
}


# DELETE devices from AzureAD, too

# authenticate
# TODO: This is causing the script to lock up when run in Windows Terminal.
#       Figure out why and fix it.
#       - It seems this might just be a little weirdsy of WT right now.
Connect-AzureAD | Out-Null

foreach ( $deviceId in $iPadList )
{
    # Get device info based on ID
    $Device = Get-AzureADDevice -Filter "DeviceID eq guid'$deviceId'"

    Write-Host -ForegroundColor Red "Deleting from AzureAD: $($Device.DisplayName)"
    Write-Host "$($Device.ObjectId)"
    Write-Host ""

    if ( -not $TestRun)
    {
        # Make the API call
        Remove-AzureADDevice -ObjectId $Device.ObjectId
    } else {
        Start-Sleep -seconds 1
    }
}
