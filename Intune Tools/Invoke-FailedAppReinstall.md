# Notes on Invoke-FailedAppReinstall.ps1

A persistent issue we face with apps deployed to iPads is that a small percentage of apps on many devices fail to download correctly, fail to create their placeholder correctly, fail to update correctly, or otherwise get into a state where they refuse to install or refuse to update.

So far the only way that I've found to deal with this *en masse* is to revoke the licenses for these failed app installs, sync the device to force the removal and reinstallation of the app or app placeholder.

This can also be done by the user, as long as the app icon is present. They can uninstall the app, and on the next sync, it will get reinstalled. However, few people notice their apps are out of date until they cause issues, so this helps to prevent us from getting into that situation.

----------------------
## Intune API References

### Retrieve VPP App List
- https://docs.microsoft.com/en-us/graph/api/intune-apps-iosvppapp-list?view=graph-rest-1.0

This is what the lines look like in the script (102-117):

```powershell
if ( $IncludePending )
{ # Then we need to include pendingInstallDeviceCount in our filter
    $uri = $baseAppUri + `
            "?filter=(installSummary/failedDeviceCount gt 0 or " + `
            "installSummary/pendingInstallDeviceCount gt 0) " + `
            "and isof('microsoft.graph.iosVppApp')"+ `
            "&expand=installSummary&select=id,displayName"

} else { # Otherwise, we can just get failed devices
    $uri = $baseAppUri + `
            "?filter=installSummary/failedDeviceCount gt 0 " + `
            "and isof('microsoft.graph.iosVppApp')"+ `
            "&expand=installSummary&select=id,displayName"
}

$vppFailedAppList = Invoke-RestMethod -Uri $uri -Headers $authToken -Method Get
```

We're using some filtering to select only apps with failed installs and to winnow down to only iOS VPP apps. Could also do this later, and omit this filtering, but I think it's better to just get what we need here if we can. We also have two different calls that we might need to make, depending on whether or not we want to included devices with the `installPending` status. These devices used to get lumped under the `failedDeviceCount`, but at some point Microsoft changed that.

MUST include the `&select` portion, as by default, the Intune API doesn't return the `installSummary`. If using `select`, then we have to select each property that we need returned to us.

URI has spaces in it!! No returns, though. I tried to break it up to be more readable.

**Watch out** for that space after the last 0 in the second line, though!! It's necessary!


**Get all apps with expanded install summary**

```powershell
$uri = $baseAppUri + "?expand=installSummary&select=id,displayName"
```
**Get all apps with no successful installs.**
*(Note that this returns apps that just have no assignments, too!)*

```powershell
$uri = $baseAppUri + "?filter=installSummary/installedDeviceCount eq 0 and isof('microsoft.graph.iosVppApp')&expand=installSummary&select=id,displayName"
```

**Get all apps with no successful installs and at least one failed install**

```powershell
$uri = $baseAppUri + "?filter=(installSummary/installedDeviceCount eq 0) and (installSummary/failedDeviceCount gt 0 or installSummary/pendingInstallDeviceCount gt 0) and isof('microsoft.graph.iosVppApp')&expand=installSummary&select=id,displayName"
```

**Get all apps with at least one successful install and at least one failed install**

```powershell
$uri = $baseAppUri + "?filter=(installSummary/installedDeviceCount gt 0) and (installSummary/failedDeviceCount gt 0 or installSummary/pendingInstallDeviceCount gt 0) and isof('microsoft.graph.iosVppApp')&expand=installSummary&select=id,displayName"
```


#### Display the data from the above queries

These will give nice tabular output of the most relevant data from the above API calls.

**List the results with the `failedDeviceCount` and `pendingInstallDeviceCount` for either of the previous two:**

```powershell
$vppFailedAppList.value | Format-Table -Property displayName,@{Name="failedDeviceCount";Expression={$_.installSummary.failedDeviceCount}},@{Name="pendingInstallDeviceCount";Expression={$_.installSummary.pendingInstallDeviceCount}}
```
**Similarly to show results w/ failed and successful device counts:**

```powershell
$vppFailedAppList.value | Format-Table -Property displayName,@{Name="failedDeviceCount";Expression={$_.installSummary.failedDeviceCount}},@{Name="pendingInstallDeviceCount";Expression={$_.installSummary.pendingInstallDeviceCount}},@{Name="installedDeviceCount";Expression={$_.installSummary.installedDeviceCount}}
```

### Revoke Licenses
https://docs.microsoft.com/en-us/graph/api/intune-apps-iosvppapp-revokedevicelicense?view=graph-rest-beta

Reminder: this link has the info about the API. HOWEVER, it has some incorrect information about the formation of the link. The link, as shown in the example POST in lacks the `/microsoft.graph.iosVppApp/` portion, and it doesn't work without it. Hence why I was getting 500 errors every time I tried doing this according to their example >:(

GET LICENSE STATUSES BY DEVICE

- https://docs.microsoft.com/en-us/graph/api/intune-apps-iosvppappassigneddevicelicense-list?view=graph-rest-betaa

Info about mobileAppInstallStatus object

- https://docs.microsoft.com/en-us/graph/api/intune-apps-mobileappinstallstatus-get?view=graph-rest-beta

Got some major additional help from a script that was shared online, which I've included in the `References` directory. This script had a different URI than the one specfified in Microsoft's documentation, and it worked, while the official URI didn't:

- `Reference script for revoking app licenses.ps1`

Additional information about this script/issue can be found here:
- https://www.reddit.com/r/Intune/comments/ecuk5x/intune_vpp_apps_not_updating_no_licenses_being/
- https://www.reddit.com/r/Intune/comments/ecuk5x/intune_vpp_apps_not_updating_no_licenses_being/fe9ch4f/

Original source here:
- https://www.codepile.net/pile/L7K6x5Wd

----------------------
### Important information about error codes

These are listed with the hexadecimal representation first. In parentheses following this is the same value interpreted as an signed int32, as this is how it gets treated by PowerShell in the data returned by the API.

It was rather difficult to track down information about what all of these meant when I was working on this, so I took the time to document what I found. As with many of Microsoft's hex error codes, there doesn't seem to be any one central list defining what all of these mean.

**UPDATE:** Microsoft has deigned to share this information with the rest of us now! They have a [help page up listing them all out](https://docs.microsoft.com/en-us/troubleshoot/mem/intune/app-install-error-codes)! How uncharacteristic! I feel like this is one of the only times I've actually seen a list like this of all these hex error codes for a particular application or service listed out all in one document.

- **0x87D13B9F** ***(-2016330849)***

  - An app update is available [...]

  - This doesn't seem to be a big issue. May need to address if these users aren't getting updates, but otherwise, this seems to be kind of a non-error right now.



- **0x87D1313D** ***(-2016333507)***

  - [UNKNOWN]

  - Has something to do with failed install, but no documentation about this one. There is a current pull request to add some info to the Intune documentation, but it's not been accepted yet. Can see it [here](https://github.com/MicrosoftDocs/IntuneDocs/issues/3253).



- **0x87D13B62** ***(-2016330910)***

  - [UNKNOWN]

  - Only saw this briefly in passing on at least one device. Didn't get any further info at that time, and the next time I checked, the device had moved to an `installPending` status.



- **0x87D13B9B** ***(-2016330853)***

  - [UNKNOWN]

  - Only showing up on one iPad. No other info.



- **0x87D11388** ***(-2016341112)***

  - [UNKNOWN]

  - [No notes yet]



- **0x87D13B6B** ***(-2016330901)***

  - [UNKNOWN]

  - [No notes yet]



- **0x87D13B7E** ***(-2016330882)***

  - [UNKNOWN]

  - [No notes yet]
