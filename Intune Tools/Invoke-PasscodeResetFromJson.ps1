<#
Takes a JSON export from cfgutil as input and invokes a passcode reset for the
iPads listed in that JSON

cfgutil command to run to get the data we neeed:
    cfgutil --format JSON -f get serialNumber

TODO: Add a parameter to just pass devices by serial number.

#>

param(
    # JSON file from cfgutil
    [string]$FilePath
)


# We'll be using Intune PowerShell Graph SDK, so we need to authenticate for that
Connect-MSGraph | Out-Null

# Get device list from Intune
$IntuneDevices = Get-IntuneManagedDevice

# Import data from JSON export from Configurator
$iPadJson = Get-Content $FilePath
$iPadInfo = $( $iPadJson | ConvertFrom-Json )

# Get all the properties of $iPadInfo.Output and then iterate over them
# Using hidden PSObject property to get to those properties as objects unto themselves
foreach ($property in $iPadInfo.Output.PSObject.Properties)
{
    if ( $property.Name -ne "Errors" )
    { # We don't want to get the error output that cfgutil puts in there at a random spot
        $iPadList += ,$property.Value.serialNumber
    }
}

# Reset passcodes for all the devices that were passed.
foreach ( $serialNumber in $iPadList )
{
    # Look up device based on serial number
    $Device = $( $IntuneDevices | Where-Object serialNumber -eq "$serialNumber" )

    Invoke-IntuneManagedDeviceResetPasscode -managedDeviceId $Device.id

}
