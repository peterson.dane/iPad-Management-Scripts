<#

.EXAMPLE
$deviceListReturn = .\Get-IntuneAppList.ps1 -Commentary -FilePath "G:\Shared drives\Tech\"
#>

[cmdletbinding()]

param(
    # Path to directory to be used for file exports; defaults to Downloads
    [string]$FilePath = "$home\Downloads",

    # Username to use to authenticate with the Intune API
    [string]$APIUsername,

    # Like -Verbose, but without everything else getting chatty
    [switch]$Commentary
)

$baseUri = "https://graph.microsoft.com/beta"
$baseDeviceUri = "$baseUri/deviceManagement/managedDevices"


# We'll be using Intune PowerShell Graph SDK, so we need to authenticate for that
Connect-MSGraph


##############################################################################
# Retrieve Device List and App Inventories
##############################################################################
# Retry on failure
$tryCount = 0
while ( $tryCount -lt 5 )
{
    try
    {
        $deviceList = Get-IntuneManagedDevice
        $tryCount = 5
    }
    catch
    {
        $tryCount++
        $deviceList = $null

        if ($Commentary) { Write-Host -ForegroundColor Yellow "API Failure: $tryCount" }

        Start-Sleep -Seconds 5
    }
}

if ($Commentary) { Write-Host "" }

# Get date now, so it's consistent
$date = Get-Date

Foreach ($device in $deviceList)
{
    # Create a new object
    $DeviceInfo = New-Object -TypeName PSObject

    # Check to see if last sync time is more than a week out
    $needsAttention = $($device.lastSyncDateTime -lt $date.AddDays(-7))
    $elapsedTime = $date.Subtract($device.lastSyncDateTime)

    # Add members and structure the object to match inventory document
    # $DeviceInfo | Add-Member -MemberType NoteProperty -Name "" -Value "$($)"
    $DeviceInfo | Add-Member -MemberType NoteProperty -Name "Device Name" -Value $device.deviceName
    $DeviceInfo | Add-Member -MemberType NoteProperty -Name "OS Version" -Value $device.osVersion
    $DeviceInfo | Add-Member -MemberType NoteProperty -Name "Needs Attention" -Value $needsAttention
    # $DeviceInfo | Add-Member -MemberType NoteProperty -Name "Jailbroken" -Value $device.jailBroken
    $DeviceInfo | Add-Member -MemberType NoteProperty -Name "Last Sync" -Value $device.lastSyncDateTime
    $DeviceInfo | Add-Member -MemberType NoteProperty -Name "Elapsed Days" -Value $elapsedTime.TotalDays
    $DeviceInfo | Add-Member -MemberType NoteProperty -Name "Enrollment Date" -Value $device.enrolledDateTime
    $DeviceInfo | Add-Member -MemberType NoteProperty -Name "Supervised" -Value $device.isSupervised
    $DeviceInfo | Add-Member -MemberType NoteProperty -Name "Model" -Value $device.model
    $DeviceInfo | Add-Member -MemberType NoteProperty -Name "Serial Number" -Value $device.serialNumber
    $DeviceInfo | Add-Member -MemberType NoteProperty -Name "Wi-Fi MAC" -Value $device.wiFiMacAddress
    $DeviceInfo | Add-Member -MemberType NoteProperty -Name "Intune Device ID" -Value $device.id
    $DeviceInfo | Add-Member -MemberType NoteProperty -Name "Azure AD Device ID" -Value $device.azureADDeviceId

    # Add new object to array
    $DeviceListProcessed += ,$DeviceInfo
}


# Export results to file
$fileBaseName = "$(Get-Date -Format "yyyy-MM-dd_HH.mm")_Enrollment-Checkup"

$csvExportPath = Join-Path $FilePath "$fileBaseName.csv"
$DeviceListProcessed | Export-Csv -Path "$csvExportPath" -NoTypeInformation -Encoding utf8

$xmlExportPath = Join-Path $FilePath "$fileBaseName.xml"
$DeviceListProcessed | Export-Clixml -Path "$xmlExportPath"
