<#
This script will get a list of VPP tokens from Intune, give the user the option
to select one interactively, and then it will cause a sync for that token.

TODO: ALMOST EVERYTHING --- This is very much a work in progress. We currently
      have just the one VPP token, so it's just as easy to run the cmdlet from
      the Intune PowerShell SDK directly. It would be good to have this properly
      developed, though.
#>

# Get list of tokens
$vppTokens = Get-IntuneVppToken

# TODO: Functionality to chooose one of them

# Sync the selected token
Invoke-IntuneVppTokenSyncLicense -vppTokenId $selectedTokenId
