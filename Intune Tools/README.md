# Intune Tools

These notes should explain some common features between all or most scripts in this directory.

Some scripts also have their own markdown-formatted files with documentation specific to them. These files will share the same base name as the script.

## Intune PowerShell SDK

The vast majority of the scripts in this directory primarily make use of Microsoft's [Intune PowerShell SDK](https://github.com/microsoft/Intune-PowerShell-SDK).

In order to authenticate with the Intune API for the Intune PowerShell SDK (IPSDK), you'll need to run this commmand:

```powershell
# We'll be using Intune PowerShell Graph SDK, so we need to authenticate for that
Connect-MSGraph
```

## Manually Constructed API Calls

Sometimes we need to make use of features that are only in the beta version of the API. Some MDM features from Apple are only present in the beta API. (These are mostly newer features.) Microsoft's documentation for the beta version of the Intune API can be found [here](https://docs.microsoft.com/en-us/graph/api/resources/intune-graph-overview?view=graph-rest-beta).

Because the IPSDK only covers v.1 of the API, in order to do this, we have to make the API calls on our own, rather than using IPSDK to make them for us. We can then construct and make our own API calls using `Invoke-WebRequest`.

The `Get-AuthToken.ps1` script allows us to get an auth token to communicate with the API. It was borrowed from a different Microsoft repository with [PowerShell sample scripts for Intune.](https://github.com/microsoftgraph/powershell-intune-samples) and was under an MIT license.

In order to get the auth token, you'll need to make sure this code is near the top of your script:

```powershell
# Get an auth token for manual Graph API calls, as we'll also be doing this
$authToken = & "$PSScriptRoot\Get-AuthToken.ps1" -User $APIUsername
```

This assumes that there's a parameter in your `param()` block named `$APIUsername`. An abridged version of that would look like this:

```powershell
param(
    # Other parameters may be here
    ⋮
    # Username to use to authenticate with the Intune API
    [string]$APIUsername,
    ⋮
    # Other parameters may be here
)
```

## JSON Input from `cfgutil`

Some scripts in here will take as input a JSON file from Apple's `cfgutil` tool. There is additional information about `cfgutil` in the main README for this repo. As a quick reference, though, the command used to get this file listing all the currently-connected devices on a Mac with Apple Configurator 2 installed is as follows:

```bash
cfgutil --format JSON -f get serialNumber
```
