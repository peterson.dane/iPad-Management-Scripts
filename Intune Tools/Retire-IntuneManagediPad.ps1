<#
Takes in an array of iPad DeviceIDs (from Intune), then takes several steps to
retire them.

TODO: If an iPad's serial number doesn't return an active device, the script should
      pass over it, rather than trying malformed API calls.

TODO: Integrate retirement by deviceId into this script, rather than having
      two separate scripts.

TODO: Rather than doing a `Where-Object` call in each separate step, the
      necessary info should all be put into one object at the very beginning.
#>

param(
    # List of serial numbers of iPads to be retired
    [string[]]$iPadList,

    # Path to JSON export from cfgutil
    # This will add entries to $iPadList, not override!
    #
    # cfgutil command to run to get the data we neeed:
    #   cfgutil --format JSON -f get serialNumber
    [string]$JsonPath,

    # Username to use to authenticate with the Intune API
    [string]$APIUsername,

    # Wait in between steps
    [int]$Wait = 300
)


$baseUri = "https://graph.microsoft.com/beta"
$deviceBaseUri = "https://graph.microsoft.com/beta/deviceManagement/managedDevices"

# We'll be using Intune PowerShell Graph SDK, so we need to authenticate for that, too
Connect-MSGraph | Out-Null

# # Wait until they've done that!
# Write-Host 'Press any key to continue...';
# $null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');


# Get an auth token for manually-constructed API calls
$authToken = & "$PSScriptRoot\Get-AuthToken.ps1" -User $APIUsername

Write-Host ""

# Get device list from Intune
$IntuneDevices = Get-IntuneManagedDevice

if ( ![string]::IsNullorWhiteSpace($JsonPath) )
{
    # Import data from JSON export from Configurator
    $iPadJson = Get-Content $JsonPath
    $iPadInfo = $( $iPadJson | ConvertFrom-Json )

    # Get all the properties of $iPadInfo.Output and then iterate over them
    # Using hidden PSObject property to get to those properties as objects unto themselves
    foreach ($property in $iPadInfo.Output.PSObject.Properties)
    {
        if ( $property.Name -ne "Errors" )
        { # We don't want to get the error output that cfgutil puts in there at a random spot
            $iPadList += ,$property.Value.serialNumber
        }
    }
}

# Revoke licenses
#   https://docs.microsoft.com/en-us/graph/api/intune-devices-manageddevice-revokeapplevpplicenses?view=graph-rest-beta
foreach ( $serialNumber in $iPadList )
{
    # Lookkup device based on serial number
    $Device = $( $IntuneDevices | Where-Object serialNumber -eq "$serialNumber" )

    Write-Host -ForegroundColor Yellow "Revoking VPP licenses for $($Device.deviceName)"
    Write-Host "$($Device.id)"
    Write-Host ""

    # Construct URL for Graph API request using the device's ID in AzureAD
    $uri="$deviceBaseUri/$($Device.id)/revokeAppleVppLicenses"

    # Make the API call
    Invoke-RestMethod -Uri $uri -Headers $authToken -Method Post
}


# # Sync devices
# foreach ( $serialNumber in $iPadList )
# {
#     # Lookkup device based on serial number
#     $Device = $( $IntuneDevices | Where-Object serialNumber -eq "$serialNumber" )
#
#     Write-Host -ForegroundColor DarkBlue "Syncing $($Device.deviceName)"
#     Write-Host "$($Device.id)"
#     Write-Host ""
#
#     # Make the API call
#     Invoke-IntuneManagedDeviceSyncDevice -managedDeviceId $Device.id
# }

if ( $Wait -gt 0 )
{
    Write-Host -foregroundcolor Blue "Waiting $Wait seconds for sync..."
    Start-Sleep -seconds $Wait
    Write-Host -foregroundcolor Blue "Moving to next step..."
    Write-Host ""
}


# Retire devices
foreach ( $serialNumber in $iPadList )
{
    # Lookkup device based on serial number
    $Device = $( $IntuneDevices | Where-Object serialNumber -eq "$serialNumber" )

    Write-Host -ForegroundColor Red "Retiring $($Device.deviceName)"
    Write-Host "$($Device.id)"
    Write-Host ""


    # Make the API call
    Invoke-IntuneManagedDeviceRetire -managedDeviceId $Device.id
}

Write-Host ""

# Sync devices again
foreach ( $serialNumber in $iPadList )
{
    # Lookkup device based on serial number
    $Device = $( $IntuneDevices | Where-Object serialNumber -eq "$serialNumber" )

    Write-Host -ForegroundColor DarkBlue "Syncing $($Device.deviceName)"
    Write-Host "$($Device.id)"
    Write-Host ""

    # Make the API call
    Invoke-IntuneManagedDeviceSyncDevice -managedDeviceId $Device.id
}

if ( $Wait -gt 0 )
{
    Write-Host -foregroundcolor Blue "Waiting $Wait seconds for sync..."
    Start-Sleep -seconds $Wait
    Write-Host -foregroundcolor Blue "Moving to next step..."
    Write-Host ""
}

Write-Host -ForegroundColor Yellow 'Check on devices in Intune. Then press any key to delete devices from Intune...';
$null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');

Write-Host -ForegroundColor Yellow 'Press any key once again to confirm deletion...';
$null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');

Write-Host ""

# DELETE devices from Intune
foreach ( $serialNumber in $iPadList )
{
    # Lookkup device based on serial number
    $Device = $( $IntuneDevices | Where-Object serialNumber -eq "$serialNumber" )

    Write-Host -ForegroundColor Red "Deleting $($Device.deviceName)"
    Write-Host "$($Device.id)"
    Write-Host ""

    # Make the API call
    Remove-IntuneManagedDevice  -managedDeviceId $Device.id
}


# DELETE devices from AzureAD, too

# authenticate
Connect-AzureAD | Out-Null

foreach ( $serialNumber in $iPadList )
{

    # Lookkup device based on serial number
    $Device = $( $IntuneDevices | Where-Object serialNumber -eq "$serialNumber" )

    # Get device info based on ID
    $ADDevice = Get-AzureADDevice -Filter "DeviceID eq guid'$($Device.id)'"

    Write-Host -ForegroundColor Red "Deleting from AzureAD: $($ADDevice.DisplayName)"
    Write-Host "$($ADDevice.ObjectId)"
    Write-Host ""

    if ( -not $TestRun)
    {
        # Make the API call
        Remove-AzureADDevice -ObjectId $ADDevice.ObjectId
    } else {
        Start-Sleep -seconds 1
    }
}
