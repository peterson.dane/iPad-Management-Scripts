<#
This script will take in list of approved apps and check if any unapproved apps
are present on any managed devices.

This script is not really relevant anymore. It had been used to check on devices
for unapproved apps when we had a slip up with the MDM. (Some devices did not
get the profile removing their App Store quickly enough after setup, and
students got unapproved apps in that time. We now avoid this by making sure the
iPad is enrolled and the App Store is gone from the home screen before we hand
it out.

.EXAMPLE
$deviceListReturn = .\Get-IntuneAppList.ps1 -Commentary -SerialPath "X:\Student_serials.txt" -FilePath "X:\App Checkup" -ApprovedAppsPath "X:\Approved__Apps_List.txt"
#>

[cmdletbinding()]

param(
    # Path to directory to be used for file exports; defaults to Downloads
    #   Should not have `\` at the end
    [string]$FilePath = "$HOME\Downloads",

    # Path to a text file with a list of *return-separated* serial numbers for
    #   iPads in it. If provided, script will discard all other devices.
    [string]$SerialPath = $null,

    # Path to a text file with a list of *return-separated* app displayName
    #   strings listed in it.
    [string]$ApprovedAppsPath = $null,

    # Path to a text file with a list of *return-separated* app displayName
    #   strings listed in it. These apps aren't actually on our approved list,
    #   technically, but we're either ignoring them or they're an alternate
    #   region version.
    [string]$IgnoredAppsPath = $null,

    # Username to use to authenticate with the Intune API
    [string]$APIUsername,

    # Like -Verbose, but without everything else getting chatty
    [switch]$Commentary
)

$baseUri = "https://graph.microsoft.com/beta"
$baseDeviceUri = "$baseUri/deviceManagement/managedDevices"


# We'll be using Intune PowerShell Graph SDK, so we need to authenticate for that
Connect-MSGraph


# Get an auth token for manual Graph API calls
$authToken = ..\Get-AuthToken.ps1 -User $APIUsername


##############################################################################
# Retrieve Device List and App Inventories
##############################################################################

# Retry a few times on failure
$tryCount = 0
while ( $tryCount -lt 5 )
{
    try
    {
        $deviceList = Get-IntuneManagedDevice
        $tryCount = 5
    }
    catch
    {
        $tryCount++
        $deviceList = $null

        if ($Commentary) { Write-Host -ForegroundColor Yellow "API Failure: $tryCount" }

        Start-Sleep -Seconds 5
    }
}

if ($Commentary) { Write-Host "" }

# Winnow out non-iOS devices
#   MIGHT NEED TO CHANGE IN FUTURE IF iPadOS GETS ITS OWN SEPARATE LISTING.
$deviceList = $($deviceList | Where-Object operatingSystem -eq "iOS")

# If $SerialPath has a value, then use that to winnow the list, too
if ( ![string]::IsNullorWhiteSpace($SerialPath) )
{
    # Make a copy of the list, and then trash the original list
    $deviceListCopy = @()
    $deviceListCopy = $deviceListCopy + $deviceList
    $deviceList = @()

    # Read in the list of serials
    $includedSerials = Get-Content -Path $SerialPath

    foreach ( $thisDevice in $deviceListCopy )
    {
        # If the S/N of this device is in the list, then include it
        if ( $includedSerials.Contains($thisDevice.serialNumber) )
        {
            if ($Commentary) { Write-Host -ForegroundColor Green "$($thisDevice.deviceName)" }
            # Then add it to the newly emptied deviceList
            $deviceList += ,$thisDevice

        } else {
            if ($Commentary) { Write-Host -ForegroundColor Red "$($thisDevice.deviceName)" }
        }
    }
    if ($Commentary) { Write-Host ""; Write-Host "" }
} # Winnowing by S/N done

foreach ( $thisDevice in $deviceList )
{
    if ($Commentary)
    {
        Write-Host -ForegroundColor DarkCyan "$($thisDevice.deviceName)"
        Write-Host -ForegroundColor DarkGray "$($thisDevice.serialNumber)"
        Write-Host ""
    }

    # Request list of apps from each device via Graph
    # Kinda sorta mentioned obliquely here, under Relationships:
    #   https://docs.microsoft.com/en-us/graph/api/resources/intune-devices-manageddevice?view=graph-rest-beta
    # This API call doesn't seem to be documented anywhere, though. CAVEAT EMPTOR!
    $uri = "$baseDeviceUri/$($thisDevice.id)/detectedApps"

    # Retry on failure
    $tryCount = 0
    while ( $tryCount -lt 5 )
    {
        try
        {
            $appList = Invoke-RestMethod -Uri $uri -Headers $authToken -Method Get
            $tryCount = 5
        }
        catch
        {
            $tryCount++
            $appList = $null

            if ($Commentary) { Write-Host -ForegroundColor Yellow "API Failure: $tryCount" }

            Start-Sleep -Seconds 5
        }
    }

    # Add the app list as a property to this device
    $thisDevice | Add-Member -MemberType NoteProperty -Name "appList" -Value $appList.value

    # # I think it's worth being a bit redundant and building an array of just
    # #   their names. Might remove if unused
    # foreach ( $app in $appList.value )
    # {
    #     $appNameList += ,$app.displayName
    # }
    #
    # # Add the array of app names as a property, too
    # $thisDevice | Add-Member -MemberType NoteProperty -Name "appNameList" -Value $appNameList
    #
    # # Clear out $appNameList for the next go-round
    # $appNameList = $null
}

##############################################################################
##############################################################################


##############################################################################
# Process this data for output
##############################################################################

# If a value for -ApprovedAppsPath was provided, read that in
if ( ![string]::IsNullorWhiteSpace($ApprovedAppsPath) )
{
    # Get list of allowed apps
    $approvedApps = Get-Content -Path "$ApprovedAppsPath" -Encoding utf8

} else {
    # Otherwise, set to empty array
    $approvedApps = @()
}

# If a value for -IgnoredAppsPath was provided, read that in
if ( ![string]::IsNullorWhiteSpace($IgnoredAppsPath) )
{
    # Get list of allowed apps
    $ignoredApps = Get-Content -Path "$IgnoredAppsPath" -Encoding utf8

} else {
    # Otherwise, set to empty array
    $ignoredApps = @()
}

# Needs to exist for first loop/lookup
$appNameList = @()

foreach ( $thisDevice in $deviceList )
{
    # Create empty object to hold the app install matrix data
    $deviceApps = New-Object -TypeName PSObject

    # Add two basic properties to the object
    $deviceApps | Add-Member -MemberType NoteProperty -Name "Name" -Value $thisDevice.deviceName
    $deviceApps | Add-Member -MemberType NoteProperty -Name "SerialNumber" -Value $thisDevice.serialNumber
    $deviceApps | Add-Member -MemberType NoteProperty -Name "UNAPPROVED APP" -Value $null

    if ($Commentary)
    {
        Write-Host ""
        Write-Host ""
        Write-Host -ForegroundColor Cyan "##############################################################################"
        Write-Host -ForegroundColor Cyan "$($thisDevice.deviceName)"
        Write-Host -ForegroundColor DarkGray "$($thisDevice.serialNumber)"
        Write-Host ""
    }

    # Now add a property with value $true for each app this device has

    Foreach ( $app in $thisDevice.appList )
    {
        $deviceApps | Add-Member -MemberType NoteProperty -Name $app.displayName -Value $true -Force

        # If the app is not on the approved list or the ignored list, set the
        #   UNAPPROVED APP property to $true

        #### NEEDS WORK: CHECK THE LOGIC HERE!!!!
        #    Should be good, but do a test run to be sure!
        if ( -not ( $approvedApps.Contains($app.displayName) -or  $ignoredApps.Contains($app.displayName) ) )
        {
            $deviceApps.("UNAPPROVED APP") = $true
        }
        #### END NEEDS WORK

        if ($Commentary)
        {
            Write-Host -ForegroundColor DarkYellow "$($app.displayName)"
        }

        # If the list of app names doesn't currently contain this app name, add it
        if ( !($appNameList.Contains($app.displayName)) )
        {
            $appNameList += ,$app.displayName

            if ($Commentary)
            {
                Write-Host -ForegroundColor DarkGray "[]+ $($app.displayName)"
            }
        }
    }

    # Add this object to the matrix
    $appInstallMatrix += ,$deviceApps
}

# Sort the list of app names alphabetically to make it more useful
$appNameList = ( $appNameList | Sort-Object )


# Need to do one more thing to make the CSV export work: make an initial blank
#   placeholder object with all apps listed as properties, then put it at the
#   beginning of the array. That way our CSV will have all the columns it needs.

if ($Commentary) { Write-Host "" }

# Create empty object
$deviceApps = New-Object -TypeName PSObject

# Add two basic properties to the object
$deviceApps | Add-Member -MemberType NoteProperty -Name "Name" -Value "APPROVED APPS"
$deviceApps | Add-Member -MemberType NoteProperty -Name "SerialNumber" -Value $null
$deviceApps | Add-Member -MemberType NoteProperty -Name "UNAPPROVED APP" -Value $null

# Populate the object
foreach ( $app in $appNameList )
{

    # If the app is on the approved  or ignored lists, set the property to $true
    # Otherwise, set it to false
    # This will help keep track in the spreadsheet once we're looking at it

    #### NEEDS WORK: Test this part! It's been updated.
    if ( $approvedApps.Contains($app) -or  $ignoredApps.Contains($app) )
    {
        $value = $true
    } else {
        $value = $false
    }
    #### END NEEDS WORK

    $deviceApps | Add-Member -MemberType NoteProperty -Name $app -Value $value

    if ($Commentary)
    {
        Write-Host -ForegroundColor Green "$($app)"
    }
}

# Add the object to the beginning of the array
$appInstallMatrix = ,$deviceApps + $appInstallMatrix

##############################################################################
##############################################################################


##############################################################################
# Output the data
##############################################################################

$exportFilePath = "$($FilePath)\$(Get-Date -Format "yyyy-MM-dd_HH.mm")_"

# Export the app install matrix as a spreadsheet
$appInstallMatrix | Export-Csv -Path "$($exportFilePath)AppInstallMatrix.csv" `
    -NoTypeInformation -Encoding utf8

# Save the whole big device list array of objects as XML so we can rehydrate it if we want
$deviceList | Export-Clixml -Path "$($exportFilePath)DeviceList_withApps.xml" -Encoding utf8

# Export the list of apps as plain text
$appNameList | Out-File -FilePath "$($exportFilePath)AppNameList.txt"

##############################################################################
##############################################################################


##############################################################################
# RETURN
##############################################################################
return $deviceList
##############################################################################
##############################################################################
