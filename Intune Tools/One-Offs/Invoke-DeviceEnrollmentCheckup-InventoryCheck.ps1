$enrollmentCheckupList="",""

$inventoryPath = ""

$iPadInventory = Import-Csv $inventoryPath

foreach ( $serialNumber in $enrollmentCheckupList )
{
    $thisiPadInfo = $( $iPadInventory | ? "Serial Number" -eq "$serialNumber" )

    Write-Host -ForegroundColor Blue "##################################################################"
    $thisiPadInfo | Format-List -Property "Asset Tag","Serial Number","Bldg","First","Last / Location","Grade / Use"

    if ($thisiPadInfo -ne $null)
    {
        $iPadList += ,$thisiPadInfo
    }
}

$iPadList | Format-Table -Property "Asset Tag","Serial Number","Bldg","First","Last / Location","Grade / Use"
