/** @OnlyCurrentDoc */

/*
Macro to automatically apply formatting to the CSV file exported by
Get-IntuneAppList.ps1 after it is opened in Google Sheets.

This was (mostly) recorded using the macro recorder in Google Sheets.
*/

function AppInstallMatrixFormatting() {
  var spreadsheet = SpreadsheetApp.getActive();
  var sheet = spreadsheet.getActiveSheet();
  sheet.getRange(1, 1, sheet.getMaxRows(), sheet.getMaxColumns()).activate();
  spreadsheet.getActiveRangeList().setFontFamily('Roboto');
  spreadsheet.getRange('B2:B190').activate();
  spreadsheet.getActiveRangeList().setFontFamily('Roboto Mono');
  spreadsheet.getRange('C:BL').activate();
  spreadsheet.getRange('C1:BL190').setDataValidation(SpreadsheetApp.newDataValidation()
  .setAllowInvalid(true)
  .requireCheckbox()
  .build());
  spreadsheet.getRange('1:1').activate();
  spreadsheet.getRange('A1:BL1').clearDataValidations();
  spreadsheet.getRange('C1:BL1').activate();
  spreadsheet.getActiveRangeList().setTextRotation(90);
  spreadsheet.getRange('C:BL').activate();
  spreadsheet.setCurrentCell(spreadsheet.getRange('BL1'));
  spreadsheet.getActiveSheet().setColumnWidths(3, 62, 43);
  spreadsheet.getActiveSheet().setColumnWidths(3, 62, 40);
  spreadsheet.getActiveSheet().setColumnWidths(3, 62, 37);
  spreadsheet.getRange('C2').activate();
  spreadsheet.getActiveSheet().setFrozenRows(2);
  spreadsheet.getActiveSheet().setFrozenColumns(3);
  spreadsheet.getRange('D:BL').activate();
  var conditionalFormatRules = spreadsheet.getActiveSheet().getConditionalFormatRules();
  conditionalFormatRules.push(SpreadsheetApp.newConditionalFormatRule()
  .setRanges([spreadsheet.getRange('D1:BL190')])
  .whenCellNotEmpty()
  .setBackground('#B7E1CD')
  .build());
  spreadsheet.getActiveSheet().setConditionalFormatRules(conditionalFormatRules);
  conditionalFormatRules = spreadsheet.getActiveSheet().getConditionalFormatRules();
  conditionalFormatRules.splice(conditionalFormatRules.length - 1, 1, SpreadsheetApp.newConditionalFormatRule()
  .setRanges([spreadsheet.getRange('D1:BL190')])
  .whenFormulaSatisfied('=D$2=TRUE')
  .setBackground('#B7E1CD')
  .build());
  spreadsheet.getActiveSheet().setConditionalFormatRules(conditionalFormatRules);
  conditionalFormatRules = spreadsheet.getActiveSheet().getConditionalFormatRules();
  conditionalFormatRules.splice(conditionalFormatRules.length - 1, 1, SpreadsheetApp.newConditionalFormatRule()
  .setRanges([spreadsheet.getRange('D1:BL190')])
  .whenFormulaSatisfied('=D$2=TRUE')
  .build());
  spreadsheet.getActiveSheet().setConditionalFormatRules(conditionalFormatRules);
  conditionalFormatRules = spreadsheet.getActiveSheet().getConditionalFormatRules();
  conditionalFormatRules.splice(conditionalFormatRules.length - 1, 1, SpreadsheetApp.newConditionalFormatRule()
  .setRanges([spreadsheet.getRange('D1:BL190')])
  .whenFormulaSatisfied('=D$2=TRUE')
  .setFontColor('#D9D9D9')
  .build());
  spreadsheet.getActiveSheet().setConditionalFormatRules(conditionalFormatRules);
  spreadsheet.getActiveSheet().autoResizeColumns(1, 1);
  spreadsheet.getActiveSheet().autoResizeColumns(2, 1);
  sheet.getRange(1, 1, sheet.getMaxRows(), sheet.getMaxColumns()).createFilter();
};
