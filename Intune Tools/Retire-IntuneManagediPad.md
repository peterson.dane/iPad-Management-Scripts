# Retire an Intune Managed iPad

This document lays out how to use the `Retire-IntuneManagediPad.ps1` and `Retire-IntuneManagediPadByDeviceId.ps1` scripts.

> ⚠ ~~**IMPORTANT NOTE:** **Currently these scripts must be run from an old-style cmdhost.exe PowerShell window ("Windows PowerShell").** They can't be run from a Windows PowerShell (PowerShell 5) tab in Windows Terminal. For some reason, the Azure Active Directory (AAD) sign-in prompt isn't appearing when either script is run from within Windows Terminal, so they just hang at that point waiting for input that never happens. You then have to manually track down and remove the device listings in AAD.~~  
**↑ This worked correctly the last time I did it, so I believe that this note is no longer relevant.**

### Retire-IntuneManagediPad.ps1

This script requires only one parameter to run, `-iPadList`, a comma-separated list of all the serial numbers of the iPads to be retired. Each serial should also be enclosed in quotation marks. The easiest way to get this list is probably to copy it out of the inventory into a text editor, find and replace all the newlines with `","`, and then add `"` at the beginning and end of the list so there aren't unpaired quotation marks.

This script also has a `-TestRun` switch parameter which will cause the script to do all the output explaining what it would do under a real run, but it will skip all the actual API calls to make changes in Intune and AAD.

There are a couple spots where the script waits for a defined amount of time before proceeding. I initially put this in because I wanted to allow time for things to sync around to the iPads, but at this point, I'm not convinced it's actually necessary to do that. If you don't specify a different time (in seconds) he default value for the `-Wait` parameter is 600 (10 minutes), but you can probably set it to something somewhat shorter. This value can always be changed permanently in the `param()` block of the script if you want it to be shorter by default.

An example of what an invocation of this script would look like:

```powershell
Retire-IntuneManagediPad.ps1 -Wait 120 -iPadList "XXXXXXXXXXXX","YYYYYYYYYYYY","ZZZZZZZZZZZZ"
```

### Retire-IntuneManagediPadByDeviceId.ps1

There's a variant of the previous script, too, `Retire-IntuneManagediPadByDeviceId.ps1`. This script allows you to specify Intune DeviceIDs (GUIDs) for the iPads instead of serial numbers. Sometimes it's more convenient to do this if that's the info that’s in front of you right now — or if an iPad has been removed and reenrolled and the serial is now attached to a new ID or to two separate IDs. You don't want to clobber an actively-used iPad! The parameters are all the same except `-iPadList` now takes a list of DeviceID values instead of serial numbers.

It's my intention to roll this into the main script, but it's not been a top priority. I've not quite decided how to structure it, and it would take some additional minor logic within the script.


### Actions Performed By These Scripts

Each script takes the following actions:

- Prompts you to sign in to the Intune API using your AAD account.

- Revokes all VPP licenses for apps on each iPad.

- Waits

- Retires each iPad. This removes our apps, configs, data, etc. on each iPad.

- Sends a sync request to each iPad to try to ensure that the iPad gets the Retire command.

- Waits

- Prompts twice for you to press any key to confirm that you want to remove the devices in Intune.

- Deletes each iPad from Intune

- Prompts you to sign into the AAD API using your AAD account.

- Deletes each iPad's object from AAD. (This entry/ID is separate from the entry in Intune, and the script has already automatically looked up the AAD ObjectID.)
