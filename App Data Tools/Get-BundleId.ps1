# Uses an app's TrackID to get the BundleID from the iTunes API. Otherwise the
#   BundleID is a bit of a pain to look up.
#
# It puts the value in your clipboard after it finds it. (Not sure if `Set-Clipboard`
#   works on macOS, though. I always run this on my Windows machine.)
#
# The TrackID is a 7-9 digit number found in the iTunes/App Store/VPP Store URL
#   for an app.
#
# VPP STORE                                       ↓↓↓↓↓↓↓↓↓ TrackID
# https://volume.itunes.apple.com/us/app/google/id284815942?term=google&ign-mpt=uo%3D4
#
# APP STORE                              ↓↓↓↓↓↓↓↓↓ TrackID
# https://apps.apple.com/us/app/google/id284815942

[cmdletbinding()]

param (
    [Parameter(Mandatory=$true)][string]$TrackID
)

# Call the iTunes API to lookup app by TrackID
$webResponse = Invoke-WebRequest -Uri "https://itunes.apple.com/lookup?id=$TrackID"

# Convert the response from the API into a PSObject
$appInfo = ConvertFrom-Json -InputObject $webResponse.Content

Write-Host ""
Write-Host -ForegroundColor Green "$($appInfo.results.bundleId)"
Write-Host ""

# Copy the bundleId to the clipboard to make things easier
Set-Clipboard -Value "$($appInfo.results.bundleId)"
