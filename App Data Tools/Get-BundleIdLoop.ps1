# Just endlessly loops the Get-BundleID script. It's handy to have as its own script.
#
# Should probably just roll this functionality into the regular command as a
#   parameter, but when I needed it, I needed it right then, so this was the
#   quickest way to get that.

while ( $true )
{
    $TrackID = Read-Host "Enter TrackID for app"

    .\Get-BundleId.ps1 -TrackID "$TrackID"
}
