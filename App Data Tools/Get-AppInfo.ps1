################################################################################
# Read in an incomplete app inventory CSV and try to fill in the blanks using
#   Apple's iTunes REST API.
#
# This was pretty much a one-time use script, but it was immensely helpful in
#   filling out our iPad app inventory document, especially when other schools
#   were merged in and we needed to get complete inventory data for all the apps
#   that were on their iPads.
#
# This script made use of Apple's iTunes REST API to get info about apps and to
#   fill out empty spots in the inventory we had.
#
# If looking up by BundleID or TrackID, only one result should be returned.
#   However, for some apps we only had names, and that search could return
#   multiple results, so in instances where there were multiple results returned
#   the script would present info about the apps and prompt the user to pick one
#   of them to use the information from.
#
################################################################################

[cmdletbinding()]
param (
    # CSV file from app inventory
    [Parameter(Mandatory=$True)][string]$FilePath,

    # Export path for the more complete app inventory produced by this script
    [Parameter(Mandatory=$True)][string]$ExportPath
)

################################################################################
# Error Checking
################################################################################

# check to be sure file exists; exit if not
if (-not $(Test-Path $FilePath)){
    throw "File `"$FilePath`" does not exist"
}

# get file info
$checkFile = dir $FilePath

# check to be sure extension is .csv; exit if not
if ($checkFile.Extension -ne ".csv") {
    throw "`"$FilePath`" has wrong extension: `"$($checkFile.Extension)`""
}

# END SETUP STAGE ##############################################################
################################################################################


Write-Verbose "Reading data from `"$FilePath`""

# Make sure that file can be opened. If it doesn't, terminate w/ error
Try {
    $incompleteAppInfo = Import-Csv $FilePath
}
Catch {
    throw "File `"$FilePath`" is not readable."
}
################################################################################

Foreach ($app in $incompleteAppInfo)
{
    if ( ![string]::IsNullorWhiteSpace($app.TrackID) )
    {
        #If we have the TrackID, lookup using that
        $webResponse = Invoke-WebRequest -Uri "https://itunes.apple.com/lookup?id=$($app.TrackID)"
        $appLookupValue = $app.TrackID

    } else {
        if ( ![string]::IsNullorWhiteSpace($app.BundleID) )
        {
            #If we have BundleID, lookup using that
            $webResponse = Invoke-WebRequest -Uri "http://itunes.apple.com/lookup?bundleId=$($app.BundleID)"
            $appLookupValue = $app.BundleID

        } else {
            # We have to do a search based on the name
            $webResponse = Invoke-WebRequest -Uri "https://itunes.apple.com/search?media=software&term=$($app.SearchTerm)"
            $appLookupValue = $app.AppName
        }
    }

    # Convert JSON response from Apple into PowerShell object
    $appInfo = ConvertFrom-Json -InputObject $webResponse.Content

    #Write-Host ""
    #Write-Host -ForegroundColor Green "$($appInfo.results.bundleId)"
    #Write-Host ""

    #Set-Clipboard -Value "$($appInfo.results.bundleId)"

    # Create object to hold data from API call
    $iPadApp = New-Object -TypeName PSObject

    # Check if we got any results in API call
    if ( $appInfo.resultCount -eq 1 ) # There is one result
    {
        # If so, let us know
        Write-Host -ForegroundColor Green "Getting data for $appLookupValue"
        Write-Host ""

        # And get the data we want
        $iPadApp | Add-Member -MemberType NoteProperty -Name "AppName" -Value "$($appInfo.results.trackName)"
        $iPadApp | Add-Member -MemberType NoteProperty -Name "SearchTerm" -Value "$($app.SearchTerm)"
        $iPadApp | Add-Member -MemberType NoteProperty -Name "BundleID" -Value "$($appInfo.results.bundleID)"
        $iPadApp | Add-Member -MemberType NoteProperty -Name "TrackID" -Value "$($appInfo.results.trackId)"
        $iPadApp | Add-Member -MemberType NoteProperty -Name "SellerName" -Value "$($appInfo.results.SellerName)"
        $iPadApp | Add-Member -MemberType NoteProperty -Name "Price" -Value "$($appInfo.results.price)"
        $iPadApp | Add-Member -MemberType NoteProperty -Name "iTunesUrl" -Value "$($appInfo.results.trackViewUrl)"

    } else {
        if ( $appInfo.resultCount -gt 1 ) # There are multiple results
        {
            # Output the names of the top ten results
            Write-Host ""
            Write-Host -ForegroundColor Green "Multiple hits for $($app.AppName):"
            Write-Host ""

            For ($i = 0; $i -le 10; ++$i)
            {
                Write-Host -ForegroundColor Cyan "$i. $($appInfo.results[$i].trackName)"
            }

            # Get input from user
            Write-Host ""
            [int]$resultNumber = Read-Host "Choose a match"
            Write-Host ""

            # Get the data we want
            $iPadApp | Add-Member -MemberType NoteProperty -Name "AppName" -Value "$($appInfo.results[$resultNumber].trackName)"
            $iPadApp | Add-Member -MemberType NoteProperty -Name "SearchTerm" -Value "$($app.SearchTerm)"
            $iPadApp | Add-Member -MemberType NoteProperty -Name "BundleID" -Value "$($appInfo.results[$resultNumber].bundleID)"
            $iPadApp | Add-Member -MemberType NoteProperty -Name "TrackID" -Value "$($appInfo.results[$resultNumber].trackId)"
            $iPadApp | Add-Member -MemberType NoteProperty -Name "SellerName" -Value "$($appInfo.results[$resultNumber].SellerName)"
            $iPadApp | Add-Member -MemberType NoteProperty -Name "Price" -Value "$($appInfo.results[$resultNumber].price)"
            $iPadApp | Add-Member -MemberType NoteProperty -Name "iTunesUrl" -Value "$($appInfo.results[$resultNumber].trackViewUrl)"

        } else { # There are zero results
            # Let us know that the app is no longer available
            Write-Host -ForegroundColor Red "No data exists for $appLookupValue"
            Write-Host ""
        }
    }

    $AppList += ,$iPadApp
}

# Export data to CSV for further use
$AppList | Export-CSV -Path "$ExportPath"
