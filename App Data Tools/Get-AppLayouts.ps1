# This was just a quick script I used to gather up some app inventory info
#   when another school was merged into ours
#
# This script was run on macOS under an account named `admin`. Make changes
#   accordingly if you ever need to run it again.
#
# This was a one-time use script to make gathering this info easier, so I don't
#   remember as much about the specifics of how I used it. But I know it takes
#   a Configurator device info CSV as one of the inputs and uses the ECID values
#   from that to address each of the plugged-in iPads. As I recall, you can't
#   just ask for this info from all the iPads; you must address them individually
#   by ECID. You could also get that info using `cfgutil`, but it was easier to
#   just use the CSV.
#
# Configurator can also put out an app inventory from all the iPads, as I recall,
#   so there must have been a specific reason we also wanted to look at the
#   homescreen layouts, too.
#
# man page for `cfgutil` can be found here: http://configautomation.com/cfgutil-man-page.html

$homeDir = "/Users/admin"
$fileDir = "$homeDir/Documents"

# If ever used again, be sure to fill in a real file name!
# This needs to be a Configurator CSV export.
$iPadListPath = "$fileDir/_____.csv"

# Import device info from file
$iPadList = Import-Csv -Path "$iPadListPath"

# Iterate through device list and get app icon layouts for each iPad.
Foreach ($iPad in $iPadList)
{
    # TODO: Should have a look and see if it's possible to get structured data
    #       out of this as a JSON as with some other options.
    cfgutil -e "$($iPad.ECID)" get-icon-layout | Out-File -Append -FilePath "$fileDir/$($iPad.Name)_$($iPad.ECID).txt"
}
