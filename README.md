# iPad-Management-Scripts

## Intune Tools

The scripts in the `Intune Tools` directory are meant to be run under Windows. In current testing, they only work under PowerShell 5 (Windows PowerShell), as the Intune PowerShell SDK seems to fail under PowerShell 6+.

There's substantially more information in the README inside that directory.

## PowerShell and macOS

Some scripts in this repo, namely those in `Name-Scripts`, are meant to be run in PowerShell on macOS. The [latest release](https://github.com/PowerShell/PowerShell/releases) can be downloaded from Microsoft via their [GitHub repo for PowerShell](https://github.com/PowerShell/PowerShell). Some other scripts may run correctly on macOS, but they've not been tested.

-----------------------

## Information about `cfgutil`

Apple Configurator 2 includes a command line utility called `cfgutil`. It allows a number of operations to be undertaken on iPads from scripts (both shell scripts and PowerShell scripts). We make moderate use of this too, mostly using it to get a list of attached iPads and to programmatically rename iPads, as Configurator doesn't include a reliable and reproduceable way to name iPads other than using their serial number.

A copy of the `cfgutil` man page can be found [here](http://configautomation.com/cfgutil-man-page.html).

Apple has documentation about the utility [here](https://support.apple.com/guide/apple-configurator-2/use-the-command-line-tool-cad856a8ea58/mac).

***Warning:*** *while that URL is still listed in Apple's [official manual for Configurator](https://support.apple.com/guide/apple-configurator-2/welcome/mac), it was dead at the time I included it here. Hopefully it's just a temporary error. There were no copies in the Wayback Machine, or I'd have included one.)*

*Just in case, I was able to get this from Google's cached copy of the page:*

> ### Use the Apple Configurator 2 command-line tool
> If you want to write shell scripts and automate specific processes, you can download the Apple Configurator 2 command-line tool.
>
> - In Apple Configurator 2, choose Install Automation Tools in the Apple Configurator 2 menu, click Install, then enter your administrator password.
>
> The command-line tool `cfgutil` is added to Apple Configurator 2, and a symbolic link is placed in `/usr/local/bin/`. After you install the command-line tool, see the `cfgutil` man page.

-----------------------

## iPad Inventory

Many of the scripts in this repository utilize a CSV file exported from an iPad
inventory. These are the columns in the iPad inventory I work from. Not all are
used in the scripts, though.

Inventory items that are used in these scripts are bolded.

- **Asset Tag**
- **DeviceName**
- **Serial Number**
- Bldg
- First
- Last / Location
- Grade / Use
- MGPx
- Purchased
- ♻ 2020
- Model
- Capacity (GB)
- WiFi MAC
- Bluetooth MAC
- UDID
- **ECID**
- Own Case
- Case Damage
- Case Description
- Damaged
- Damage Description
- Damage Preexisting
- Repair incidents
- Comments
- Repair Comments
- Old AT
- Label Text

-----------------------

## Terms used in these scripts and their comments

- **MEM Portal** - this is used pretty synonymously with "Intune Portal". The Microsoft Endpoint Manager Portal is where you access Intune on the web.

- **DEP** - **D**evice **E**nrollment **P**rogram. This is a service offered through Apple School Manager to automatically enroll devices in an MDM when they're set up. See [Apple's documentation](https://support.apple.com/en-us/HT204142) for more info.
