[cmdletbinding()]
param (
    # FilePath parameter tells where .csv input file is
    # Not sure about the Mandatory attribute's behavior in script. May want to try this out...
    # If keeping Mandatory attribute, get rid of the "= $(throw ...)" portion?
    [Parameter(Mandatory=$True)][string]$FilePath
)

if (-not $(Test-Path $filePath)){
    throw "File `"$filePath`" does not exist"
}

# get file info
$checkFile = dir $filePath

# check to be sure extension is .csv; exit if not
if ($checkFile.Extension -ne ".csv") {
    throw "`"$filePath`" has wrong extension: `"$($checkFile.Extension)`""
}

# Make sure that file can be opened. If it doesn't, terminate w/ error
Try {
    $idList = Import-Csv $filePath
}
Catch {
    throw "File `"$filePath`" is not readable."
}

# Iterate through device records and apply name to iPads
Foreach ($iPad in $idList)
{
    Write-Host ""
    Write-Host -Foregroundcolor Cyan "-----------------------------------------"
    Write-Host -Foregroundcolor Cyan "Setting name: $($iPad.DeviceName)"

    #/bin/echo
    cfgutil -e "$($iPad.ECID)" rename "$($iPad.DeviceName)"
}
