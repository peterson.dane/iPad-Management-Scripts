The two files in here differ only in a couple of properties names used on objects.

Back when Rename-iPads.ps1 was structured differently (using pre-made CSV files which were subsets of the full inventory, rather than by detecting which devices were plugged in and looking just those up), it made some sense to have some alternate ways to quickly apply names.

These are retained only for reference.
