<#
This script uses the provided path to a CSV copy of an iPad inventory to rename
all iPads plugged into a macOS device with Apple Configurator 2 installed.

See the README at the root of this repo for more information about the
requirements for this inventory.

The script uses `cfgutil` to get a list of attached iPads, looks them up in the
inventory by serial number, and then uses `cfgutil` to rename each iPad.

#>

param(
    # Location of inventory CSV file.
    # May want to set this to default to wherever you keep that info on the
    #   system you'll be using for this.
    [string]$InventoryPath =
)


# Then try to automatically name them!

# Import the full inventory
$iPadInventory = Import-Csv $InventoryPath

# Get info for connected iPads
$iPadJson  = cfgutil --format JSON -f get serialNumber

# Convert that info from JSON to PSObject
$iPadInfo = $( $iPadJson | ConvertFrom-Json )

# Get all the properties of $iPadInfo.Output and then iterate over them.
#   Using hidden PSObject property to get to those properties as objects
#   unto themselves
foreach ($property in $iPadInfo.Output.PSObject.Properties)
{
    if ( $property.Name -ne "Errors" )
    { # We don't want to get the error output that cfgutil puts in there at a random spot
        $iPadList += ,$property.Value.serialNumber

        # Find this iPad in the inventory
        $thisiPadInfo = $( $iPadInventory | Where-Object "Serial Number" -eq "$($property.Value.serialNumber)" )

        Write-Host ""
        Write-Host -Foregroundcolor Gray "------------------------------------------"
        Write-Host -Foregroundcolor Cyan "Setting name: $($thisiPadInfo.DeviceName)"

        # Then rename the iPad
        cfgutil -e "$($property.Name)" rename "$($thisiPadInfo.DeviceName)"
    }
}

Write-Host -Foregroundcolor Gray "------------------------------------------"
Write-Host -Foregroundcolor Gray "|  Finished running rename iPads script  |"
Write-Host -Foregroundcolor Gray "------------------------------------------"

# Comment this out if you don't want the Mac verbally announcing the script's completion.
say "Finished running Rename iPads Script."
